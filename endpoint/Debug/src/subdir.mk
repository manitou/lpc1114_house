################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/cr_startup_lpc11xx.c \
../src/crp.c \
../src/ext.c \
../src/irq_handlers.c \
../src/main.c \
../src/temp.c 

OBJS += \
./src/cr_startup_lpc11xx.o \
./src/crp.o \
./src/ext.o \
./src/irq_handlers.o \
./src/main.o \
./src/temp.o 

C_DEPS += \
./src/cr_startup_lpc11xx.d \
./src/crp.d \
./src/ext.d \
./src/irq_handlers.d \
./src/main.d \
./src/temp.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M0 -D__USE_CMSIS=CMSIS_CORE_LPC11xx -D__LPC11XX__ -I"C:\LPC1114_GIT\rfm73_client" -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx" -I"C:\LPC1114_GIT\core" -I"C:\LPC1114_GIT\endpoint\inc" -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -std=c99 -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


