/*
 * temp.c
 *
 *  Created on: 22.09.2014
 *      Author: Admin
 */
#include "LPC11xx.h"
#include "gpio.h"
#include "ssp1.h"
#include "temp.h"

void temp_init(){
	SSP1_IOConfig();
	SSP1_Init();
}

uint16_t temp(){
	uint16_t ret = 0;
	SSP1_cs(0);
	ret |= SSP1_txrx(0xff)<<8;
	ret |= SSP1_txrx(0xff);
	SSP1_cs(1);
	return (ret * 625)/1000;
}
