/*
 * door.c
 *
 *  Created on: 15.04.2014
 *      Author: Admin
 */

#include "LPC11xx.h"
#include "ext.h"
#include "leds.h"
#include "gpio.h"
#include "printf.h"
#include "adc_utils.h"

void ext_init(){
	LPC_IOCON->PIO0_3 = (1 << 4)|(1 << 7); //gpio mode, pullup
	GPIOInit();
	GPIOSetDir(0, 3, 0);
}

uint8_t ext_int(){
	return (LPC_GPIO[0]->MASKED_ACCESS[(1<<3)]) ? 1 : 0;
}

