/*
 * ap_090.h
 *
 *  Created on: 07.08.2014
 *      Author: Admin
 */

#ifndef AP_090_H_
#define AP_090_H_
#include <stdint.h>

void ap_init(void);
void ap_connect(uint8_t id);
void ap_disconnect(void);
uint8_t ap_set_brightness(uint8_t addr, uint8_t _br);
void ap_send(uint8_t *data, uint8_t len);
uint8_t ap_recv(uint8_t *buf);
void ap_poll(void);

#endif /* AP_090_H_ */
