/*
 * ap_090.c
 *
 *  Created on: 07.08.2014
 *      Author: Admin
 */

#include "ap_090.h"
#include <string.h>
#include "LPC11xx.h"
#include "printf.h"
#include "counter.h"

uint8_t state = 0;
uint8_t id = 0;
uint8_t _to_connect = 0;
uint32_t _connect_time = 0;

uint8_t r_write(uint8_t *buf, uint8_t len){
	while(len--){
		uart_putchar(*(buf++));
	}
	return 1;
}

uint8_t r_read(uint8_t *buf, uint8_t len){
	uint8_t in_len = 0;
	in_len = uart_rx_ready();
	if(len > in_len){
		//not ready yet
		return 0;
	}
	if(!uart_rx(buf, in_len)){
		//some error
		return 0;
	}
	return in_len;
}

void ap_init(){
	state = 0;
	id = 0;
}

void ap_connect(uint8_t _id){
	//static uint32_t timeout;
	//uint8_t buf[10] = {0x3D, 0x02, 0x00, 0x01, 0x00, 0xC2, 0x01, 0x00, 0x00, 0x00};
	//uint8_t ret[30] = {0};
	//timeout = gettc();
	//buf[3] = _id;
	//send query
	//r_write(buf, 8);
	_to_connect = _id;
	//wait for answer
	//while(!r_read(ret, 26) && timeout + 500 > gettc());
	//parse answer
	//return id = ret[3];
}

void ap_disconnect(void){
	//static uint32_t timeout;
	uint8_t buf[10] = {0x3D, 0x02, 0x00, 0x01, 0xFF, 0xC2, 0x00, 0x01, 0x00, 0x00};
	//uint8_t ret[10] = {0};
	//timeout = gettc();
	//buf[3] = id;
	//send query
	r_write(buf, 8);
	//wait for answer
	//while(!r_read(ret, 8) && timeout + 500 > gettc());
	//id = 0;
}

void ap_send(uint8_t *data, uint8_t len){

}

uint8_t ap_recv(uint8_t *buf){
	return 0;
}

uint8_t ap_set_brightness(uint8_t addr, uint8_t _br){
	//static uint32_t timeout;
	uint8_t buf[10] = {0x3D, 0x03, 0x00, 0x00, 0x22, 0x00, 0xC2, 0x23, 0x00, 0x00};
	//uint8_t ret[10] = {0};
//	if(!id)
//		return 0;
	//timeout = gettc();
	//buf[3] = id;
	buf[3] = addr;
	buf[5] = _br;
	buf[7] = 0x22 + _br + addr;
	//send query
	r_write(buf, 9);
	//wait for answer
	//while(!r_read(ret, 8) && timeout + 500 > gettc());
	//parse answer
	return 1;
}

//uint8_t relay_get(){
//	static uint32_t timeout;
//	uint8_t buf[10] = {0x3D, 0x02, 0x00, 0x01, 0x20, 0xC2, 0x21, 0x00, 0x00, 0x00};
//	timeout = gettc();
//	//send query
//	r_write(buf, 8);
//	//wait for answer
//	buf[5] = 0;
//	while(!r_read(buf, 9) && timeout + 500 > gettc());
//	//parse answer
//	return buf[5];
//}
//
//uint8_t relay_set(uint8_t bitmask){
//	static uint32_t timeout;
//	uint8_t buf[10] = {0x3D, 0x02, 0x00, 0x01, 0x21, 0x00, 0xC2, 0x22, 0x00, 0x00};
//	buf[5] = bitmask;
//	timeout = gettc();
//	//send query
//	r_write(buf, 9);
//	buf[5] = 0;
//	//wait for answer
//	while(!r_read(buf, 9) && timeout + 500 > gettc());
//	if(buf[5] == bitmask){
//		return 1;
//	}
//	return 0;
//}

void ap_poll(){
	static uint8_t _wait_len = 0;
	if(!_wait_len){
		if(uart_rx_ready() >= 3){
			uint8_t incoming[3] = {0};
			uart_rx(incoming, 3);
			if(incoming[0] == 0x3D){
				_wait_len = incoming[1] + 3;
			}
		}
	}else{
		if(uart_rx_ready() >= _wait_len){
			uint8_t incoming[50];
			uart_rx(incoming, _wait_len);
			if(incoming[_wait_len-3] == 0xC2){
				//gotcha
				printf("AP: Incoming 0x%02x\r\n", incoming[1]);
				_wait_len = 0;
				switch(incoming[1]){
				case 0x00: //CONNECT
//					id = incoming[0];
//					_to_connect = 0;
					break;
				case 0xFF: //DISCONNECT
					//id = 0;
					break;
				case 0x22: //BRIGHTNESS ACK
					break;
				default:
					break;
				}
			}else{
				//flush buffer
				while(uart_rx_ready()){
					uart_rx(incoming, 1);
				}
			}
		}
	}
//	if(!id && _to_connect){
//		if(gettc() > _connect_time + 5000 || _connect_time == 0){
//			//connecting
//			uart_printf("AP Connect\r\n");
//			uint8_t buf[10] = {0x3D, 0x02, 0x00, 0x01, 0x00, 0xC2, 0x01, 0x00, 0x00, 0x00};
//			buf[3] = _to_connect;
//			stick_wait(1);
//			r_write(buf, 8);
//			_connect_time = gettc();
//		}
//	}
}
