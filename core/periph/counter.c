#include "LPC11xx.h"
#include "counter.h"

volatile uint32_t _seconds = 0;
volatile uint32_t _mseconds = 0;

void TIMER16_0_IRQHandler(void)
{
  _mseconds++;
  if(!(_mseconds%1000))
	  _seconds++;
  LPC_TMR16B0->IR = 0xff;
}

static void _counter_init(){
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<7);
	LPC_TMR16B0->TCR = 0x02;		/* reset timer */
	LPC_TMR16B0->PR  = 48; 			/* set prescaler to get 1 M counts/sec */
	LPC_TMR16B0->MR0 = 1000;		/* set to 1msec interrupt */
	LPC_TMR16B0->IR  = 0xff;		/* reset all interrrupts */
	LPC_TMR16B0->MCR = 0x03;		/* int and reset on match */
	NVIC_ClearPendingIRQ(TIMER_16_0_IRQn);
	NVIC_EnableIRQ(TIMER_16_0_IRQn);
}

void counter_init_12mhz(){
	_counter_init();
	LPC_TMR16B0->PR  = 12; 			/* set prescaler to get 1 M counts/sec */
	LPC_TMR16B0->TCR = 0x01;		/* start timer */
}

void counter_init_48mhz(){
	_counter_init();
	LPC_TMR16B0->PR  = 48; 			/* set prescaler to get 1 M counts/sec */
	LPC_TMR16B0->TCR = 0x01;		/* start timer */
}

void counter_stop(){
	LPC_TMR16B0->TCR = 0x02;		/* reset timer */
	NVIC_ClearPendingIRQ(TIMER_16_0_IRQn);
	NVIC_DisableIRQ(TIMER_16_0_IRQn);
	_mseconds = 0;
	_seconds = 0;
}

uint32_t count_1khz(){
	return _mseconds;
}

uint32_t count_32khz(){
	return _mseconds*32;
}
uint32_t count_1hz(){
	return _seconds;
}
