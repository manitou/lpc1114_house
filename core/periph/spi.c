/*
 * spi_nrf.c
 *
 *  Created on: 28.11.2013
 *      Author: Admin
 */

#include "LPC11xx.h"
#include "spi.h"


void nrf_start(){
	//GPIOSetValue(0, 7, 1);
	LPC_GPIO0->MASKED_ACCESS[(1 << 7)] = 1 << 7;
}

void nrf_stop(){
	//GPIOSetValue(0, 7, 0);
	LPC_GPIO0->MASKED_ACCESS[(1 << 7)] = 0;
}

void nrfspi_init(){
	uint8_t i = 8;
	uint8_t temp;


	LPC_SYSCON->PRESETCTRL |= (1 << 0)|(1 << 2); /* disable reset */
	LPC_SYSCON->SYSAHBCLKCTRL |= (0x1<<11); /* enable SSP0 clock */
	LPC_SYSCON->SSP0CLKDIV = 0x01;			/* Divided by 2 */
	LPC_IOCON->PIO0_2 &= ~0x07;
	//LPC_IOCON->PIO0_2 |= 0x01;		/* SPI SSEL */

	LPC_IOCON->SCK_LOC = 0x02;		/* SPI SCK on P0.6 */
	LPC_IOCON->PIO0_6 &= ~0x07;
	LPC_IOCON->PIO0_6 |= 0x02;		/* SPI SCK */
	LPC_IOCON->PIO0_8 &= ~0x07;
	LPC_IOCON->PIO0_8 |= 0x09;		/* SPI MISO */
	LPC_IOCON->PIO0_9 &= ~0x0F;
	LPC_IOCON->PIO0_9 |= 0x09;		/* SPI MOSI */

	LPC_SSP0->CR0 = 0x0707;			/* SPI 8bit len, SCR is 15 */
	LPC_SSP0->CR1 = 0x0;	/* master mode, disabled */
	LPC_SSP0->CPSR = 0x2;
	LPC_SSP0->IMSC = 0x0;			/* disable all interrupts */
	LPC_SSP0->CR1 |= (1 << 1); 		/* enable SPI controller */
	while(i--){
		temp = LPC_SSP0->DR;
	}
	//SSP_Init(0);
	//GPIOSetDir(0, 2, 1);
	LPC_GPIO0->DIR |= 1 << 2;
	//GPIOSetValue(0, 2, 1);
	LPC_GPIO0->MASKED_ACCESS[(1<<2)] = (1<<2);
	LPC_IOCON->PIO0_7 &= ~0x07;
	LPC_IOCON->PIO0_7 |= 0x08;
	//GPIOSetDir(0, 7, 1);			/* radio enable pin */
	LPC_GPIO0->DIR |= 1 << 7;
	//GPIOSetValue(0, 7, 0);
	LPC_GPIO0->MASKED_ACCESS[(1<<7)] = 0;
}

uint8_t nrfspi_txrx(uint8_t txbyte){
	while (!(LPC_SSP0->SR & SSPSR_TFE));
	LPC_SSP0->DR = (uint16_t)txbyte;
	while (LPC_SSP0->SR & SSPSR_BSY);
	//while ( (LPC_SSP0->SR & (SSPSR_BSY|SSPSR_RNE)) != SSPSR_RNE );
	return (uint8_t)(LPC_SSP0->DR & 0xff);
	//return SSP0_TxRx(txbyte);
}

void nrfspi_select(){
	//GPIOSetValue(0, 2, 0);
	LPC_GPIO0->MASKED_ACCESS[(1<<2)] = 0;
}

void nrfspi_release(){
	//GPIOSetValue(0, 2, 1);
	LPC_GPIO0->MASKED_ACCESS[(1<<2)] = (1<<2);
}
