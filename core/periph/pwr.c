/*
 * pwr.c
 *
 *  Created on: 24.02.2014
 *      Author: Admin
 */
#include "LPC11xx.h"
#include "pwr.h"
#include "gpio.h"
#include "leds.h"
#include "printf.h"
#include "counter.h"

uint8_t on_wdt_clk = 0;

uint8_t *wdt_flag = 0;

//uint32_t ahbclkctrl = 0;

volatile uint32_t ms_cycles = 3125; // 3125 for 12MHz, 12500 for 48MHz, 80 for 8kHz

static void down_gpio(){
	LPC_GPIO0->DIR  = 0x1;
	LPC_GPIO1->DIR  = 0x0; // Set all GPIO pins as inputs //do not set as output!
	LPC_GPIO2->DIR  = 0x0; // Set all GPIO pins as inputs
	LPC_GPIO3->DIR  = 0x0; // Set all GPIO pins as inputs
	LPC_GPIO0->DATA = 1|(1 << 4)|(1 << 5)|(1 << 6)|(1 << 11);     	// all low exept reset, clkout, scl, sda, sck0, ad0
	LPC_GPIO1->DATA = (1 << 3)|(1 << 11);     								// all low exept swdio, ad7
	LPC_GPIO2->DATA = (1 << 1);												// all low exept sck1
	LPC_GPIO3->DATA = 0;
	LPC_IOCON->RESET_PIO0_0 = 0x10; //reset mode, pullup
	LPC_IOCON->PIO0_1 = 0x12; //MAT mode, pullup
	LPC_IOCON->PIO0_2 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO0_3 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO0_4 = 0x0; //PIO mode
	LPC_IOCON->PIO0_5 = 0x0; //PIO mode
	LPC_IOCON->PIO0_6 = 0x10; //PIO mode, pullup
	LPC_IOCON->PIO0_7 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO0_8 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO0_9 = 0x8; //PIO mode, pulldown
	LPC_IOCON->SWCLK_PIO0_10 = 0x9; //PIO mode, pulldown
	LPC_IOCON->R_PIO0_11 = 0x91; //PIO mode, pullup, digital
	LPC_IOCON->R_PIO1_0 = 0x89; //PIO mode, pulldown, digital
	LPC_IOCON->R_PIO1_1 = 0x89; //PIO mode, pulldown, digital
	LPC_IOCON->R_PIO1_2 = 0x89; //PIO mode, pulldown, digital
	LPC_IOCON->SWDIO_PIO1_3 = 0x91; //PIO mode, pullup, digital
	LPC_IOCON->PIO1_4 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO1_5 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO1_6 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO1_7 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO1_8 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO1_9 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO1_10 = 0x8; //PIO mode, pulldown, digital
	LPC_IOCON->PIO1_11 = 0x90; //PIO mode, pullup, digital
	LPC_IOCON->PIO2_0 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_1 = 0x10; //PIO mode, pulldown
	LPC_IOCON->PIO2_2 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_3 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_7 = 0x8; //PIO mode, pulldown
// dummy pins
	LPC_IOCON->PIO2_4 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_5 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_6 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_8 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_9 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_10 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO2_11 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO3_0 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO3_1 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO3_2 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO3_3 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO3_4 = 0x8; //PIO mode, pulldown
	LPC_IOCON->PIO3_5 = 0x8; //PIO mode, pulldown
}

static void up_gpio(uint32_t ahbclkctrl){
	LPC_IOCON->RESET_PIO0_0 = 0x10; //RESET mode, pullup
	LPC_IOCON->SWCLK_PIO0_10 = 0x8; //SWCLK mode, pulldown
	LPC_IOCON->SWDIO_PIO1_3 = 0x90; //SWDIO mode, pullup, digital
	GPIOInit();
	GPIOSetDir(1, 0, 1);
	GPIOSetDir(1, 1, 1);
	if(ahbclkctrl & (1 << 5)){
		//I2C
		LPC_IOCON->PIO0_4 = 1; //SCL
		LPC_IOCON->PIO0_5 = 1; //SDA
	}
	if(ahbclkctrl & (1 << 7)){
		//TC16B0
	}
	if(ahbclkctrl & (1 << 8)){
		//TC16B1
	}
	if(ahbclkctrl & (1 << 9)){
		//TC32B0
	}
	if(ahbclkctrl & (1 << 10)){
		//TC32B1
	}
	if(ahbclkctrl & (1 << 11)){
		//SPI0
		LPC_IOCON->PIO0_8           |= 0x09;		/* SSP MISO */
		LPC_IOCON->PIO0_9           |= 0x09;		/* SSP MOSI */
		//LPC_IOCON->SWCLK_PIO0_10 |= 0x02;				/* SSP CLK */
		//LPC_IOCON->PIO2_11 = 0x01;					/* SSP CLK */
		LPC_IOCON->PIO0_6 = 0x02;					/* SSP CLK */
		LPC_IOCON->SCK_LOC = 2; //0 - PIO0_10, 1 - PIO2_11, 2 - PIO0_6
		GPIOSetDir(0, 2, 1 );					/* SSP SSEL */
		GPIOSetValue(0, 2, 1 );
		LPC_IOCON->PIO0_7 |= 0x08;
		GPIOSetDir(0, 7, 1);			/* radio enable pin */
		GPIOSetValue(0, 7, 0);
	}
	if(ahbclkctrl & (1 << 12)){
		//UART
		LPC_IOCON->PIO1_6 |= 0x01;     /* UART RXD */
		LPC_IOCON->PIO1_7 |= 0x01;     /* UART TXD */
	}
	if(ahbclkctrl & (1 << 13)){
		//ADC
	}
	if(ahbclkctrl & (1 << 17)){
		//CAN
	}
	if(ahbclkctrl & (1 << 18)){
		//SPI1
		LPC_IOCON->PIO2_2 |= 0x02;		/* SSP MISO */
		LPC_IOCON->PIO2_3 |= 0x02;		/* SSP MOSI */
		LPC_IOCON->PIO2_1 |= 0x02;		/* SSP CLK */
		GPIOSetDir(2, 8, 1);			/* SSP SSEL */
		GPIOSetValue(2, 8, 1);
	}
}


void pwr_init(){
	//need for controller start
	LPC_SYSCON->SYSAHBCLKDIV  = 1; //initial startup
	LPC_SYSCON->SYSAHBCLKCTRL = 0x1005f;
	pwr_wdt_init();
	counter_init_12mhz();
	uart_12mhz();
	//pwr_clkout(1, CLK_MAIN);

}

void asm_wait_nonblock(uint16_t _10_ms, uint8_t *flag){
	if(wdt_flag)
		return;
	wdt_flag = flag;
	*flag = 0;
	pwr_wdt_start(_10_ms/10);
}

void asm_wait_cancel(){
	wdt_flag = 0;
	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_DisableIRQ(WDT_IRQn);
}

void asm_wait(uint16_t _10_ms){
	uint16_t n;
	while(_10_ms--){
		n = ms_cycles;
		while(n--){
			__NOP();
		}
	}
}

void pwr_clkout(uint8_t enable, enum clkout source){
	//make CLKOUT
	enable &= 0x01;
	LPC_IOCON->PIO0_1 &= ~(0x07);
	LPC_IOCON->PIO0_1 |= enable | 0x8; //set PIO0_1 as CLKOUT
	if(enable){
		LPC_SYSCON->CLKOUTCLKSEL = source; /* main clk */
		LPC_SYSCON->CLKOUTUEN = 0x00;
		LPC_SYSCON->CLKOUTUEN = 0x01;
		LPC_SYSCON->CLKOUTDIV = 1;
		while(!(LPC_SYSCON->CLKOUTUEN&0x01)); /* wait for clock switched */
		LPC_SYSCON->CLKOUTDIV = LPC_SYSCON->SYSAHBCLKDIV;
	}
}

void pwr_switch_to_48MHz(){
	LPC_SYSCON->PDRUNCFG     &= ~((1 << 5)|(1 << 7)); //Enable System osc and PLL
	LPC_SYSCON->SYSOSCCTRL = 0; //12Mhz crystal resonator
	LPC_SYSCON->SYSPLLCLKSEL = 1; //system osc as PLL input
	LPC_SYSCON->SYSPLLCLKUEN  = 0x01; //toggle
	LPC_SYSCON->SYSPLLCLKUEN  = 0x00;
	LPC_SYSCON->SYSPLLCLKUEN  = 0x01;
	while (!(LPC_SYSCON->SYSPLLCLKUEN & 0x01));

	LPC_SYSCON->SYSPLLCTRL = 0x23; //set to 48MHz
	while (!(LPC_SYSCON->SYSPLLSTAT & 0x01));

	LPC_SYSCON->MAINCLKSEL = 0x03; /* switch to PLL out */
	LPC_SYSCON->MAINCLKUEN = 0x00;
	LPC_SYSCON->MAINCLKUEN = 0x01;
	while(!(LPC_SYSCON->MAINCLKUEN&0x01));
	ms_cycles = 12500;
	counter_init_48mhz();
	uart_48mhz();
}

void pwr_switch_to_12MHz(){
	LPC_SYSCON->MAINCLKSEL = 0x00; /* switch to IRC */
	LPC_SYSCON->MAINCLKUEN = 0x00;
	LPC_SYSCON->MAINCLKUEN = 0x01;
	while(!(LPC_SYSCON->MAINCLKUEN&0x01));
	LPC_SYSCON->PDRUNCFG |= ((1 << 5)|(1 << 7)); //Disable System osc and PLL
	ms_cycles = 3125;
	counter_init_12mhz();
	uart_12mhz();
}

void pwr_switch_to_8KHz(){
	LPC_SYSCON->MAINCLKSEL = 0x02; /* switch to WDT */
	LPC_SYSCON->MAINCLKUEN = 0x00;
	LPC_SYSCON->MAINCLKUEN = 0x01;
	while(!(LPC_SYSCON->MAINCLKUEN&0x01));
	LPC_SYSCON->PDRUNCFG |= ((1 << 5)|(1 << 7)); //Disable System osc and PLL
	ms_cycles = 1;
	//stop timer
	counter_stop();
	uart_8khz();
}

uint8_t pwr_is_8KHz(){
	return (ms_cycles == 1);
}

void pwr_wdt_init(){
	/* Enable clock to WDT */
	LPC_SYSCON->SYSAHBCLKCTRL |= (1<<15);
	LPC_SYSCON->PDRUNCFG &= ~(0x1<<6); /* enable WDT power */
	LPC_SYSCON->WDTOSCCTRL = 0x03F; /* ~9.3kHz */
	//LPC_SYSCON->WDTOSCCTRL = 0x1e1; /* ~1MHz */
	LPC_SYSCON->WDTCLKDIV = 0x01; /* ??? check for CLKOUT same freq when 1 and 255 */
	LPC_SYSCON->WDTCLKSEL = 0x02; /* WDT osc */
	LPC_SYSCON->WDTCLKUEN = 0x00; /* toggle register to switch clock source */
	LPC_SYSCON->WDTCLKUEN = 0x01;
	while(!(LPC_SYSCON->WDTCLKUEN&0x01)); /* wait for clock switched */

	//LPC_WDT->TC = 0x138ff; /* ~ 80100 ~ 10seconds */
	//LPC_WDT->TC = 0x9CFF; /* ~40000 ~ 5 seconds */
	LPC_WDT->TC = 0x8FF; /* ~ 1 second */
	LPC_WDT->WARNINT = 0x0;
	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_DisableIRQ(WDT_IRQn);
}

void pwr_wdt_start(uint16_t _100_ms){
	LPC_WDT->TC = _100_ms*0xFF; /* ~ 0.12 second */
	LPC_WDT->MOD |= 0x01; /* set WDEN */
	LPC_WDT->FEED = 0xAA; /* feed the dog */
	LPC_WDT->FEED = 0x55;
	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_EnableIRQ(WDT_IRQn);
}

static void pwr_set_timer(uint16_t _10ms){
	LPC_IOCON->PIO0_1 &= ~0x7;
	LPC_IOCON->PIO0_1 |= 0xA; //set PIO 0_1 as timer output with pulldown CT32B0_MAT2
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 9);
	LPC_TMR32B0->TCR |= (1 << 1); //reset
	LPC_TMR32B0->IR = 0xff;
	LPC_TMR32B0->CTCR = 0; //timer mode
	LPC_TMR32B0->PR = 0; //set some value if prescaling of timer is needed
	LPC_TMR32B0->MCR = 0x6 << 6; // reset and stop on MR2  //interrupt
	LPC_TMR32B0->MR2 = 93*_10ms; //match value!!!! sets delay!!!
	LPC_TMR32B0->EMR = 2 << 8; //set 0 to TMR32B0_MAT2 pin on match //set 1
	LPC_TMR32B0->TCR = 1; // start
}

//#define MOVE
#define DOOR

void pwr_deepsleep(uint16_t _100_ms){
//enter DEEPSLEEP
	//DEEPSLEEP is only be interrupted by external pin
	//external pin can only be emulated by timer (NOT watchdog).
	//So we have to set timer to wake us. Damn!
	uint32_t ahbclkctrl = LPC_SYSCON->SYSAHBCLKCTRL;
	LPC_PMU->PCON &= ~(1 << 1); /* enter sleep modes on WFI */
	//LPC_SYSCON->PDSLEEPCFG = 0x18BF; /* WDT on BOD off */
	LPC_SYSCON->PDSLEEPCFG = 0xFFBF; /* test, same as default */
	LPC_SYSCON->PDAWAKECFG = LPC_SYSCON->PDRUNCFG;
	SCB->SCR |= (1 << 2); /* deepsleep instead of sleep */
	LPC_SYSCON->STARTAPRP0 = (1 << 1)|(1 << 3);
	LPC_SYSCON->STARTERP0 = (1 << 1)|(1 << 3);
	pwr_switch_to_8KHz();
	LPC_SYSCON->SYSAHBCLKCTRL = 0x1825F; //0x1825F; //minimum + IOCON + WDT + TC32B0 + GPIO(leds)
	pwr_clkout(0, CLK_MAIN);
	down_gpio();
	pwr_set_timer(_100_ms*10);
	LPC_SYSCON->STARTRSRP0CLR = (1 << 1);
	LPC_SYSCON->STARTRSRP0CLR = (1 << 3);
	NVIC_ClearPendingIRQ(WAKEUP1_IRQn);
	NVIC_EnableIRQ(WAKEUP1_IRQn);
	NVIC_ClearPendingIRQ(WAKEUP3_IRQn);
	NVIC_EnableIRQ(WAKEUP3_IRQn);
	LPC_SYSCON->SYSAHBCLKCTRL &= ~(1 << 6);
	__WFI();
	LPC_SYSCON->SYSAHBCLKCTRL = ahbclkctrl;
	up_gpio(ahbclkctrl);
}

uint8_t pwr_is_event_wake(){
	return (LPC_TMR32B0->MR2 != LPC_TMR32B0->TC);
}

void WAKEUP_IRQHandler(void){
	__disable_irq();
	NVIC_ClearPendingIRQ(WAKEUP1_IRQn);
	NVIC_DisableIRQ(WAKEUP1_IRQn);
	NVIC_ClearPendingIRQ(WAKEUP3_IRQn);
	NVIC_DisableIRQ(WAKEUP3_IRQn);
	LPC_SYSCON->STARTRSRP0CLR = (1 << 1)|(1 << 3);
	LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 6);
	LPC_TMR32B0->TCR = 0; // stop
	SCB->SCR &= ~(1 << 2); /* sleep instead of deepsleep */
	pwr_switch_to_12MHz();
	pwr_clkout(1, CLK_MAIN);
	__enable_irq();
}

void WDT_IRQHandler(void){
	__disable_irq();
	//printf("WDT INT!\r\n");
	if(wdt_flag)
		*wdt_flag = 1;
	LPC_WDT->MOD &= ~(1 << 2); //clear OF
	LPC_WDT->MOD &= ~(1 << 3); //and INT flags
	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_DisableIRQ(WDT_IRQn);
	__enable_irq();
}
