################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../periph/adc.c \
../periph/adc_utils.c \
../periph/counter.c \
../periph/gpio.c \
../periph/i2c.c \
../periph/leds.c \
../periph/loopbuffer.c \
../periph/pwr.c \
../periph/spi.c \
../periph/ssp1.c 

OBJS += \
./periph/adc.o \
./periph/adc_utils.o \
./periph/counter.o \
./periph/gpio.o \
./periph/i2c.o \
./periph/leds.o \
./periph/loopbuffer.o \
./periph/pwr.o \
./periph/spi.o \
./periph/ssp1.o 

C_DEPS += \
./periph/adc.d \
./periph/adc_utils.d \
./periph/counter.d \
./periph/gpio.d \
./periph/i2c.d \
./periph/leds.d \
./periph/loopbuffer.d \
./periph/pwr.d \
./periph/spi.d \
./periph/ssp1.d 


# Each subdirectory must supply rules for building sources it contributes
periph/%.o: ../periph/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDUMMY_PRINTF -DDEBUG -D__CODE_RED -DCORE_M0 -D__USE_CMSIS=CMSIS_CORE_LPC11xx -D__LPC11XX__ -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx\inc" -I"C:\LPC1114_GIT\core" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -std=c99 -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


