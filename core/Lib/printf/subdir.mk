################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../printf/printf.c \
../printf/small_printf_float.c \
../printf/small_printf_nofloat.c \
../printf/small_printf_support.c \
../printf/small_utils.c 

OBJS += \
./printf/printf.o \
./printf/small_printf_float.o \
./printf/small_printf_nofloat.o \
./printf/small_printf_support.o \
./printf/small_utils.o 

C_DEPS += \
./printf/printf.d \
./printf/small_printf_float.d \
./printf/small_printf_nofloat.d \
./printf/small_printf_support.d \
./printf/small_utils.d 


# Each subdirectory must supply rules for building sources it contributes
printf/%.o: ../printf/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDUMMY_PRINTF -DDEBUG -D__CODE_RED -DCORE_M0 -D__USE_CMSIS=CMSIS_CORE_LPC11xx -D__LPC11XX__ -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx\inc" -I"C:\LPC1114_GIT\core" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -std=c99 -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


