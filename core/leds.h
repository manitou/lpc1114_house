/*
 * leds.h
 *
 *  Created on: 23.12.2013
 *      Author: Admin
 */

#ifndef LEDS_H_
#define LEDS_H_

/* LEDs
 * LED indication control system.
 */

#include <stdint.h>
typedef enum{
	LED1 = 0,
	LED2 = 1
}leds_t;

// LED system init function
void led_init(void);

// Turn ON led (LED1 or LED2)
void led_on(leds_t led);

// Turn OFF led (LED1 or LED2)
void led_off(leds_t led);

// Make "blink" with LED (LED1 or LED2)
// Function is blocking safe so can be used even in interrupts.
void led_blink(leds_t led);

// LED system polling function.
// Used to make LED system work as designed and have to be polled
// repeatedly in main loop.
void led_poll(void);


#endif /* LEDS_H_ */
