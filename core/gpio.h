/*****************************************************************************
 *   $Id:: gpio.h 4790 2010-09-03 23:35:38Z nxp21346                        $
 *   Project: NXP LPC11xx software example
 *
 *   Description:
 *     This file contains definition and prototype for GPIO.
 *
 ****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
****************************************************************************/
#ifndef __GPIO_H 
#define __GPIO_H
#include <stdint.h>
#include "LPC11xx.h"

/* GPIO
 * Standard peripheral wrapper for general purpose inputs or outputs access.
 */

#define PORT0		0
#define PORT1		1
#define PORT2		2
#define PORT3		3
static LPC_GPIO_TypeDef (* const LPC_GPIO[4]) = { LPC_GPIO0, LPC_GPIO1, LPC_GPIO2, LPC_GPIO3 };

// GPIO system init function
void GPIOInit( void );

// GPIO Interrupt configuring function
// Args:
//	portNum - port number (0..3)
//	bitPosi - pin number (0..11)
//	sense 	- edge or level sense (0 - edge, 1 - level)
//	single	- single or double edge sense (0 - single, 1 - double)
//	event	- event trigger source level (0 - low or edge fall, 1 - high or edge rise)
void GPIOSetInterrupt( uint32_t portNum, uint32_t bitPosi, uint32_t sense,
		uint32_t single, uint32_t event );

// GPIO Interrupt enable function
// Args:
//	portNum - port number (0..3)
//	bitPosi - pin number (0..11)
void GPIOIntEnable( uint32_t portNum, uint32_t bitPosi );

// GPIO Interrupt disable function
// Args:
//	portNum - port number (0..3)
//	bitPosi - pin number (0..11)
void GPIOIntDisable( uint32_t portNum, uint32_t bitPosi );

// GPIO Interrupt state check function
// Args:
//	portNum - port number (0..3)
//	bitPosi - pin number (0..11)
// Returns: 1 - interrupt pending, 0 - no interrupt
uint32_t GPIOIntStatus( uint32_t portNum, uint32_t bitPosi );

// GPIO Pending Interrupt clear function
// Args:
//	portNum - port number (0..3)
//	bitPosi - pin number (0..11)
void GPIOIntClear( uint32_t portNum, uint32_t bitPosi );

// GPIO Value change function
// Args:
//	portNum - port number (0..3)
//	bitPosi - pin number (0..11)
//	bitVal	- output value (0 - low, 1 - high)
void GPIOSetValue( uint32_t portNum, uint32_t bitPosi, uint32_t bitVal );


uint8_t GPIOGetValue( uint32_t portNum, uint32_t bitPosi);

// GPIO output toggle state function - inverts output state on pin
// Args:
//	portNum - port number (0..3)
//	bitPosi - pin number (0..11)
void GPIOToggle( uint32_t portNum, uint32_t bitPosi);

// GPIO Pin direction change function
// Args:
//	portNum - port number (0..3)
//	bitPosi - pin number (0..11)
//	dir		- direction (0 - input, 1 - output)
void GPIOSetDir( uint32_t portNum, uint32_t bitPosi, uint32_t dir );

#endif /* end __GPIO_H */
/*****************************************************************************
**                            End Of File
******************************************************************************/
