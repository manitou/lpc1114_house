/*
 * wdt.h
 *
 *  Created on: 24.02.2014
 *      Author: Admin
 */

#ifndef PWR_H_
#define PWR_H_

/* PWR
 * Power and clock control module. Allows to change main clock frequency
 * block program run for a time and enter deepsleep mode.
 * As main initialization system, HAVE TO BE CALLED FIRST.
 */

enum clkout{
	CLK_IRC = 0,
	CLK_SYSOSC = 1,
	CLK_WDT = 2,
	CLK_MAIN = 3
};

// Main initialization function.
// HAVE TO BE CALLED AT VERY START
// By default system start working at 12MHz
void pwr_init(void);

// Controls debug CLKOUT pin.
// Args:
//	enable 		- enables or disables CLKOUT (0 - off, 1 - on)
//	source		- selects CLKOUT source:
//					CLK_IRC - internal RC osc (~12MHz)
//					CLK_SYSOSC - external system osc
//					CLK_WDT - watchdog osc (~8KHz)
//					CLK_MAIN - main clock
void pwr_clkout(uint8_t enable, enum clkout source);

// Switch main clock to 48MHz
void pwr_switch_to_48MHz(void);

// Switch main clock to 12MHz
void pwr_switch_to_12MHz(void);

// Switch main clock to 8KHz
// Warning! Debug information is disabled at 8KHz
void pwr_switch_to_8KHz(void);

// Check if system is running at 8KHz. Deprecated.
uint8_t pwr_is_8KHz(void);

// Watchdog system init. For inner usage. Do not call directly.
void pwr_wdt_init(void);

// Watchdog start function. For inner usage. Do not call directly.
void pwr_wdt_start(uint16_t _100_ms);

// Enter deepsleep mode.
// Args:
//	_100_ms		- sleep delay in 100ms ticks (10*100ms = 1 second)
void pwr_deepsleep(uint16_t _100_ms);

// Checks if wake was caused by external event or wake timer.
// Returns: 0 - waked by timer, 1 - external wake
uint8_t pwr_is_event_wake(void);

// Blocking wait function. Blocks program run for defined time.
// Please avoid using it in main loop. Make counter based delays.
// Args:
//	_10_ms		- wait time in 10ms ticks (100*10ms = 1 second)
void asm_wait(uint16_t _10_ms);

// Non-blocking wait (WDT based, so only one at once). Wait function
// based on watchdog. Sets flag to 1 when delay time expires. Can be used
// only once at a time.
// PLEASE, USE COUNTER BASED DELAYS. This delay is unstable.
// Args:
//	_10_ms		- wait time in 10ms ticks
//	flag		- pointer to flag witch will be changed
void asm_wait_nonblock(uint16_t _10_ms, uint8_t *flag);

// Non-blocking wait cancel function.
void asm_wait_cancel(void);

#endif /* PWR_H_ */
