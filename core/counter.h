
#include <stdint.h>

/* Counter
 * General purpose counter. Used to make timeouts in non-blocking
 * systems. Based on 16bit timer/counter.
 */

// Basic init and reinit function when working at 12MHz clock.
// Can be used for switching between frequencies.
void counter_init_12mhz(void);

// Basic init and reinit function when working at 48MHz clock.
void counter_init_48mhz(void);

// Stops counter. Not used anywhere.
void counter_stop(void);

// Returns count of 1KHz ticks from start of system.
// Value can be overflown, so use this counter only for short
// delays. For long delay use count_1hz().
uint32_t count_1khz(void);

// Returns count of 32KHz ticks from start of system.
// Made for compatibility with ENC28J60 library. Please,
// use count_1khz() instead of this.
uint32_t count_32khz(void);

// Returns uptime of system in seconds. Also used to make long
// non-blocking delays or timeouts.
uint32_t count_1hz(void);
