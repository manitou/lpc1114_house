/*
 * loopbuffer.h
 *
 *  Created on: 26.02.2014
 *      Author: Admin
 */

#ifndef LOOPBUFFER_H_
#define LOOPBUFFER_H_
#include <stdint.h>

/* LoopBuffer
 * LoopBuffer - my lightweight implementation of circle FIFO buffer.
 * Is used for making some stacks and queues. Can be dynamically
 * created or deleted. Have to be allocated with lb_alloc() before first use.
 * Loopbuffer is allocated with chunk size. After alloc it works only with
 * such chunks.
 */

// defines maximum buffers count
#define LOOPBUFFERS_COUNT 2
// defines each buffer size in bytes
#define LOOPBUFFER_BYTESIZE 200

//tested 25_03

// Loopbuffer init system. must be called at program start
void lb_init(void);

// Allocates loopbuffer with fixed chunk size
// Args:
//	struct_size - size of one chunk, cannot be greater than LOOPBUFFER_BYTESIZE
// Returns: id of allocated buffer, must be used in each function
uint8_t lb_alloc(size_t struct_size);

// Checks buffer for any data in it
// Args:
//	num 		- ID of allocated buffer
// Returns: 0 - if buffer contains data, 1 - buffer is empty
uint8_t lb_is_free(uint8_t num);

// Checks if buffer is full. Though buffer is circular, so it can be written into
// without an error. But data in it can be overwritten, to avoid this, please
// check if buffer have extra space.
// Args:
//	num 		- ID of allocated buffer
// Returns: 0 - if buffer have extra place, 1 - buffer is full
uint8_t lb_is_full(uint8_t num);

// Puts data into buffer
// Args:
//	num 		- ID of allocated buffer
//	data		- pointer to start of data. data array have to be less or equal to
//					allocated struct_size
// Returns: 0 when writing to uninitialzied buffer, 1 on success write
uint8_t lb_put(uint8_t num, void *data);

// Puts data into buffer
// Args:
//	num 		- ID of allocated buffer
//	data		- pointer to start of data. data array have to be greater or equal to
//					allocated struct_size to fit read data
// Returns: 0xFF when reading from uninitialzied buffer or data is NULL,
//			0 when buffer is empty, 1 on success read
uint8_t lb_get(uint8_t num, void *data);

// Deallocates buffer
// Args:
//	num			- ID of allocated buffer
// Returns: 0 when ID is wrong, 1 - othrewise
uint8_t lb_free(uint8_t num);

// Removes all data from buffer
// Args:
//	num			- ID of allocated buffer
// Returns: 0 when ID is wrong, 1 - othrewise
uint8_t lb_clean(uint8_t num);

#endif /* LOOPBUFFER_H_ */
