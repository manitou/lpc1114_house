/*
 * printf_uart.h
 *
 *  Created on: Nov 15, 2013
 *      Author: manitou
 */

#ifndef PRINTF_H_
#define PRINTF_H_
#include <stdarg.h>
#include <stdint.h>

/* Printf
 * Printing and string processing system.
 * Used to transfer debug or commands through UART @ 115200bps.
 * Can be operational at both 12 and 48 MHz, at 8 Mhz UART system is disabled.
 * By default is used to display debug information. To disable whole module
 * (with sprintf() function) please define DUMMY_PRINTF in library project.
 */

enum{
	CLK_48MHZ = 0,
	CLK_12MHZ = 1,
	CLK_8KHZ = 2
};

//Used to disable debug output locally, deprecated
//#define DUMMY_PRINTF

// UART Initialization function
// Args:
//	clk		- current clock frequency (CLK_48MHZ | CLK_12MHZ | CLK_8KHZ)
void uart_init(uint8_t clk);

// Inner UART frequency switch functions. Used by PWR module. Do not use directly.
void uart_48mhz(void);
void uart_12mhz(void);
void uart_8khz(void);

// Simplified output function. For inner use. Do not call directly.
void uart_putstr(int zero, char *p, int len);

// Function that prints character to UART. For inner usage, but can be used on demand.
// Args:
//	p		- character to print
int uart_putchar(char p);

// Common formatted string display function. Acts like usual printf, but without floating
// point support.
void printf(const char *format, ...);

// "Light" analog of library function. Also without floating point support.
void sprintf(char *buf, const char *format, ...);

// Converts string number (base 10) representation into unsigned integer. Can work with
// not null-terminated strings.
// Args:
//	from_str - input string
//	to_num	 - pointer where to put result
// Returns: 0 when input string is not a number, 1 on succesful conversion
uint8_t atoi(uint8_t *from_str, uint16_t *to_num);

// Check if UART buffer has any pending data
// Returns: amount of bytes pending
uint8_t uart_rx_ready(void);

// Reads data from UART input buffer
// Args:
//	buf		- pointer where to put received data
// 	maxlen	- count of bytes to read
// Returns: actual count of readden bytes
uint8_t uart_rx(uint8_t *buf, uint8_t maxlen);

// Printf testing function. Not for direct usage.
void printf_test(void);

#endif /* PRINTF_H_ */
