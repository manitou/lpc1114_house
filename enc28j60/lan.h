#pragma once

#include <string.h>
#include <stdint.h>

/* LAN
 * This is TCP/IP stack written specially for ENC28J60 Ethernet driver
 * by LifeLover (http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-testy-proizvoditelnosti-i-kratkoe-opisanie-api-steka.html)
 * It was severely damadged by me, but in common it's same. Next time
 * it have to be refactored to hide all low level features.
 * Now this is most "dangerous" part of library. Please don't ask how it works.
 * AND DON'T EDIT!!!! It will be commented in future
 */

/*
 * Options
 */
// Main settings area, let's leave all enabled.
#define WITH_ICMP
#define WITH_DHCP
#define WITH_UDP
#define WITH_TCP
//#define WITH_TCP_REXMIT

/*
 * Config
 */

#define MAC_ADDR				{0x00,0x13,0x37,0x01,0x23,0x50}

#ifndef WITH_DHCP
#	define IP_ADDR				inet_addr(192,168,0,223)
extern uint32_t ip_addr;
#	define IP_SUBNET_MASK		inet_addr(255,255,255,0)
#	define IP_DEFAULT_GATEWAY	inet_addr(192,168,0,1)
#endif

#define ARP_CACHE_SIZE			3

#define IP_PACKET_TTL			64

#define TCP_MAX_CONNECTIONS		5
#define TCP_WINDOW_SIZE			65535
#define TCP_SYN_MSS				512
#ifdef WITH_TCP_REXMIT
#	define TCP_REXMIT_TIMEOUT	1000
#	define TCP_REXMIT_LIMIT		5
#else
#	define TCP_CONN_TIMEOUT		1000000
#endif


/*
 * BE conversion
 */

#define htons(a)			((((a)>>8)&0xff)|(((a)<<8)&0xff00))
#define ntohs(a)			htons(a)

#define htonl(a)			( (((a)>>24)&0xff) | (((a)>>8)&0xff00) |\
								(((a)<<8)&0xff0000) | (((a)<<24)&0xff000000) )
#define ntohl(a)			htonl(a)

#define inet_addr(a,b,c,d)	( ((uint32_t)a) | ((uint32_t)b << 8) |\
								((uint32_t)c << 16) | ((uint32_t)d << 24) )

/*
 * Ethernet
 */

#define ETH_TYPE_ARP		htons(0x0806)
#define ETH_TYPE_IP			htons(0x0800)

typedef struct eth_frame {
	uint8_t to_addr[6];
	uint8_t from_addr[6];
	uint16_t type;
	uint8_t data[];
}__attribute__((packed)) eth_frame_t;

/*
 * ARP
 */

#define ARP_HW_TYPE_ETH		htons(0x0001)
#define ARP_PROTO_TYPE_IP	htons(0x0800)

#define ARP_TYPE_REQUEST	htons(1)
#define ARP_TYPE_RESPONSE	htons(2)

typedef struct arp_message{
	uint16_t hw_type;
	uint16_t proto_type;
	uint8_t hw_addr_len;
	uint8_t proto_addr_len;
	uint16_t type;
	uint8_t mac_addr_from[6];
	uint32_t ip_addr_from;
	uint8_t mac_addr_to[6];
	uint32_t ip_addr_to;
}__attribute__((packed)) arp_message_t;

typedef struct arp_cache_entry {
	uint32_t ip_addr;
	uint8_t mac_addr[6];
}__attribute__((packed)) arp_cache_entry_t;

/*
 * IP
 */

#define IP_PROTOCOL_ICMP	1
#define IP_PROTOCOL_TCP		6
#define IP_PROTOCOL_UDP		17

typedef struct ip_packet{
	uint8_t ver_head_len;
	uint8_t tos;
	uint16_t total_len;
	uint16_t fragment_id;
	uint16_t flags_framgent_offset;
	uint8_t ttl;
	uint8_t protocol;
	uint16_t cksum;
	uint32_t from_addr;
	uint32_t to_addr;
	uint8_t data[];
}__attribute__((packed)) ip_packet_t;


/*
 * ICMP
 */

#define ICMP_TYPE_ECHO_RQ	8
#define ICMP_TYPE_ECHO_RPLY	0

typedef struct icmp_echo_packet{
	uint8_t type;
	uint8_t code;
	uint16_t cksum;
	uint16_t id;
	uint16_t seq;
	uint8_t data[];
}__attribute__((packed)) icmp_echo_packet_t;


/*
 * UDP
 */

typedef struct udp_packet{
	uint16_t from_port;
	uint16_t to_port;
	uint16_t len;
	uint16_t cksum;
	uint8_t data[];
}__attribute__((packed)) udp_packet_t;


/*
 * TCP
 */

#define TCP_FLAG_URG		0x20
#define TCP_FLAG_ACK		0x10
#define TCP_FLAG_PSH		0x08
#define TCP_FLAG_RST		0x04
#define TCP_FLAG_SYN		0x02
#define TCP_FLAG_FIN		0x01

typedef struct tcp_packet{
	uint16_t from_port;
	uint16_t to_port;
	uint32_t seq_num;
	uint32_t ack_num;
	uint8_t data_offset;
	uint8_t flags;
	uint16_t window;
	uint16_t cksum;
	uint16_t urgent_ptr;
	uint8_t data[];
}__attribute__((packed)) tcp_packet_t;

#define tcp_head_size(tcp)	(((tcp)->data_offset & 0xf0) >> 2)
#define tcp_get_data(tcp)	((uint8_t*)(tcp) + tcp_head_size(tcp))

typedef enum tcp_status_code {
	TCP_CLOSED,
	TCP_SYN_SENT,
	TCP_SYN_RECEIVED,
	TCP_ESTABLISHED,
	TCP_FIN_WAIT
} tcp_status_code_t;

typedef struct tcp_state {
	tcp_status_code_t status;
	uint32_t event_time;
	uint32_t seq_num;
	uint32_t ack_num;
	uint32_t remote_addr;
	uint16_t remote_port;
	uint16_t local_port;
#ifdef WITH_TCP_REXMIT
	uint8_t is_closing;
	uint8_t rexmit_count;
	uint32_t seq_num_saved;
#endif
} tcp_state_t;

typedef enum tcp_sending_mode {
	TCP_SENDING_SEND,
	TCP_SENDING_REPLY,
	TCP_SENDING_RESEND
} tcp_sending_mode_t;

#define TCP_OPTION_PUSH			0x01
#define TCP_OPTION_CLOSE		0x02
//manitou
#define TCP_OPTION_SEND			0x04


/*
 * DHCP
 */

#define DHCP_SERVER_PORT		htons(67)
#define DHCP_CLIENT_PORT		htons(68)

#define DHCP_OP_REQUEST			1
#define DHCP_OP_REPLY			2

#define DHCP_HW_ADDR_TYPE_ETH	1

#define DHCP_FLAG_BROADCAST		htons(0x8000)

#define DHCP_MAGIC_COOKIE		htonl(0x63825363)

typedef struct dhcp_message{
	uint8_t operation;
	uint8_t hw_addr_type;
	uint8_t hw_addr_len;
	uint8_t unused1;
	uint32_t transaction_id;
	uint16_t second_count;
	uint16_t flags;
	uint32_t client_addr;
	uint32_t offered_addr;
	uint32_t server_addr;
	uint32_t unused2;
	uint8_t hw_addr[16];
	uint8_t unused3[192];
	uint32_t magic_cookie;
	uint8_t options[];
}__attribute__((packed)) dhcp_message_t;

#define DHCP_CODE_PAD			0
#define DHCP_CODE_END			255
#define DHCP_CODE_SUBNETMASK	1
#define DHCP_CODE_GATEWAY		3
#define DHCP_CODE_REQUESTEDADDR	50
#define DHCP_CODE_LEASETIME		51
#define DHCP_CODE_MESSAGETYPE	53
#define DHCP_CODE_DHCPSERVER	54
#define DHCP_CODE_RENEWTIME		58
#define DHCP_CODE_REBINDTIME	59

typedef struct dhcp_option{
	uint8_t code;
	uint8_t len;
	uint8_t data[];
}__attribute__((packed)) dhcp_option_t;

#define DHCP_MESSAGE_DISCOVER	1
#define DHCP_MESSAGE_OFFER		2
#define DHCP_MESSAGE_REQUEST	3
#define DHCP_MESSAGE_DECLINE	4
#define DHCP_MESSAGE_ACK		5
#define DHCP_MESSAGE_NAK		6
#define DHCP_MESSAGE_RELEASE	7
#define DHCP_MESSAGE_INFORM		8

typedef enum dhcp_status_code {
	DHCP_INIT,
	DHCP_ASSIGNED,
	DHCP_WAITING_OFFER,
	DHCP_WAITING_ACK
} dhcp_status_code_t;


//Some variable export, next time well work with them through setters/getters.
#ifdef WITH_DHCP
extern dhcp_status_code_t dhcp_status;
extern uint32_t dhcp_retry_time;
extern uint32_t dhcp_renew_time;
#endif
extern uint32_t ip_addr;
extern uint32_t ip_mask;
extern uint32_t ip_gateway;

/*
 * LAN
 */

// Main data buffer. Used for some low-level magic. Don't repeat at home.
extern volatile uint8_t net_buf[];

extern uint8_t linkstate;

// LAN calls
//Main init function.
void lan_init(void);

// Polling function. Call it in main loop to make things work.
void lan_poll(void);

//Shows if link is up.
uint8_t lan_up(void);

// ICMP calls
// Sends PING. Please use lu_ping() instead.
uint8_t icmp_send(uint32_t dest_ip);

// ICMP callback
// defined in lan_utils.c
void icmp_packet(eth_frame_t *frame);

// UDP callback
// not defined yet. I left it for UDP debug.
void udp_packet(eth_frame_t *frame, uint16_t len);

// UDP calls
// UDP send. You have to have filled UDP frame to use it.
uint8_t udp_send(eth_frame_t *frame, uint16_t len);
// Can be called only from udp_packet() callback.
void udp_reply(eth_frame_t *frame, uint16_t len);

// TCP callbacks
// All TCP callbacks defined in mqtt_wrapper.c
// Returns 1 on connection accept, otherwise 0.
uint8_t tcp_listen(uint8_t id, eth_frame_t *frame);
void tcp_read(uint8_t id, eth_frame_t *frame, uint8_t re);
void tcp_write(uint8_t id, eth_frame_t *frame, uint16_t len);
void tcp_closed(uint8_t id, uint8_t hard);

// TCP calls
// Opens connection, returns some type of socket.
uint8_t tcp_open(uint32_t addr, uint16_t port, uint16_t local_port);
// Sends data, you have to have active socket and filled frame to use it.
void tcp_send(uint8_t id, eth_frame_t *frame, uint16_t len, uint8_t options);
// Returns status of TCP socket.
tcp_status_code_t tcp_get_status(uint8_t id);
// Resets all TCP sockets.
void tcp_reset(void);

// ARP resolv
// For inner use.
uint8_t *arp_resolve(uint32_t node_ip_addr);
