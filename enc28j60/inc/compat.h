/*
 * nxp_compat.h
 *
 *  Created on: 23.12.2013
 *      Author: Admin
 */

#ifndef NXP_COMPAT_H_
#define NXP_COMPAT_H_
#include <stdint.h>
#include "counter.h"

#define gettc() count_32khz()
#define rtime() count_1hz()

void spi_init(void);
void spi_cs(uint8_t enable);
uint8_t spi_txrx(uint8_t data);


#endif /* NXP_COMPAT_H_ */
