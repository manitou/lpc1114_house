/*
 * nxp_compat.c
 *
 *  Created on: 23.12.2013
 *      Author: Admin
 */
#include "LPC11xx.h"
#include "compat.h"
#include "ssp1.h" // not usual SSP lib, only for ENC24J60

void spi_init(){
	SSP1_IOConfig();
	SSP1_Init();
}

void spi_cs(uint8_t enable){
	SSP1_cs(enable);
}

uint8_t spi_txrx(uint8_t data){
	return SSP1_txrx(data);
}

void wait(uint16_t mseconds){
	uint32_t timestamp = gettc();
	while(gettc() - timestamp < mseconds);
}
