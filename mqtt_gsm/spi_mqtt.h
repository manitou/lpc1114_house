/*
 * spi_mqtt.h
 *
 *  Created on: 09.01.2015
 *      Author: Admin
 */

#ifndef SPI_MQTT_H_
#define SPI_MQTT_H_
#include <stdint.h>

//Callback argument structure. Please define subscribe callbacks as follow:
//	void subscribe_function(mqtt_sub_callback_t *data);
typedef struct{
	// Topic from witch message was received.
	char *topic;
	// Message body.
	char *msg;
}mqtt_sub_callback_t;

// For inner usage.
typedef struct{
	char topic[20];
	void (*callback)(mqtt_sub_callback_t*);
}mqtt_sub_t;

void sm_init(void);
void sm_poll(void);
void sm_subscribe(char *topic, void (*callb)(mqtt_sub_callback_t*));
void sm_publish(char *topic, char *msg);
extern void spisl_int(void);

#endif /* SPI_MQTT_H_ */
