/*
 * spi_mqtt.c
 *
 *  Created on: 09.01.2015
 *      Author: Admin
 */

#include <string.h>
#include "LPC11xx.h"
#include "pwr.h"
#include "printf.h"
#include "spi_slave.h"
#include "counter.h"
#include "spi_mqtt.h"

uint8_t data[100];
uint32_t status_timeout = 0;
uint8_t sock_ready = 0;
mqtt_sub_t _subs[5];
uint8_t _subs_num = 0;

int sm_callback(uint8_t *_data, uint8_t len){
	printf("SPI callback\r\n");
	switch(_data[0]){
	case 0x01: //status
		printf("GSM Status: GSM %s Socket %s\r\n",
				(_data[1] ? "active" : "inactive"),
				(_data[2] ? "active" : "inactive"));
		sock_ready = _data[2];
		break;
	case 0x02: //publish
		printf("Incoming message: ");
		for(uint8_t i = 0; i < len; i++){
			printf("0x%02x ", _data[i]);
		}
		printf("\r\n");
		// 0x02 0x07 0x53 0x2f 0x33 0x33 0x33 0x2f 0x32 0x02 0x31 0x34
		uint8_t *topic = (_data+2);
		uint8_t *msg = (_data + 3 + _data[1]);
		*(msg + *(msg-1)) = 0x00;
		*(topic + _data[1]) = 0x00;
		printf("Message: ");
		for(uint8_t i = 0; i < len; i++){
			printf("0x%02x ", _data[i]);
		}
		printf(" %s, %s\r\n", topic, msg);
		void (*callb)(mqtt_sub_callback_t*) = NULL;
		for(uint8_t i = 0; i < 5; i++){
			if(!(_subs[i].topic[0]))
				continue;
			char *plus = strchr(_subs[i].topic, '+');
			if(plus){
				//mask parsing
				if(!memcmp(_subs[i].topic, topic, (plus - _subs[i].topic))){
					//gotcha
					callb = _subs[i].callback;
					break;
				}
			}else{
				if(!memcmp(_subs[i].topic, topic, _data[1])){
					//gotcha
					callb = _subs[i].callback;
					break;
				}
			}
		}
		if(!callb)
			return 0;
		mqtt_sub_callback_t callb_data;
		callb_data.topic = (char*)topic;
		callb_data.msg = (char*)msg;
		callb(&callb_data);
		break;
	default:
		return 0;
	}
	return 1;
}

void sm_init(){
	spisl_init();
	spisl_set_callback(&sm_callback);
	memset(data, 0x00, 100);
	memset(_subs, 0x00, sizeof(_subs));
}

void sm_send_subscribe(mqtt_sub_t *sub){
	printf("Subscribing to topic \"%s\"\r\n", sub->topic);
	data[0] = 0x03;
	data[1] = strlen(sub->topic);
	memcpy((data+2), sub->topic, strlen(sub->topic));
	spisl_send(data, strlen(sub->topic)+2);
	memset(data, 0x00, 100);
}

void sm_poll(){
	static uint32_t sub_timeout = 0;
	static uint8_t subscribed = 0;
	spisl_poll();
	if(count_1khz() - status_timeout > 3000){
		status_timeout = count_1khz();
		printf("Status query\r\n");
		data[0] = 0x01;
		spisl_send(data, 1);
	}
	if(subscribed < _subs_num)
		if(count_1khz() - sub_timeout > 1000){
			printf("Sub: %u %u\r\n", subscribed, _subs_num);
			sub_timeout = count_1khz();
			sm_send_subscribe(_subs+subscribed);
			subscribed++;
		}
}

void sm_subscribe(char *topic, void (*callb)(mqtt_sub_callback_t*)){
	if(_subs_num == 5)
		return;
	mqtt_sub_t *sub = &(_subs[_subs_num]);
	strcpy(sub->topic, topic);
	sub->callback = callb;
	_subs_num++;
}

void sm_publish(char *topic, char *msg){
	uint8_t *p = data;
	printf("Publishing \"%s\" to \"%s\"\r\n", msg, topic);
	*p++ = 0x02;
	*p++ = strlen(topic);
	memcpy(p, topic, strlen(topic));
	p += strlen(topic);
	*p++ = strlen(msg);
	memcpy(p, msg, strlen(msg));
	p += strlen(msg);
	spisl_send(data, p - data);
	memset(data, 0x00, 100);
}

