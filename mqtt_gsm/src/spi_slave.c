/*
 * spi_slave.c
 *
 *  Created on: 07.01.2015
 *      Author: Admin
 */
#include <string.h>
#include "LPC11xx.h"
#include "pwr.h"
#include "gpio.h"
#include "printf.h"
#include "spi_slave.h"
#include "counter.h"
#include "leds.h"

void int_recv(void);
void int_send(void);

spi_pipe_t rx, tx;
int (*ssp_callback)(uint8_t *buf, uint8_t len);
uint32_t int_timer = 0;
uint8_t __int = 0;

void spisl_init(void){
	uint8_t i, Dummy =Dummy;
	LPC_SYSCON->PRESETCTRL |= (0x1<<2);
	LPC_SYSCON->SYSAHBCLKCTRL |= (0x1<<18);//clock SSP1
	LPC_SYSCON->SSP1CLKDIV = 0x8;            //Divided by 2
	LPC_IOCON->PIO2_2 &= ~0x07;            //SSP I/O config
	LPC_IOCON->PIO2_2 |= 0x02;                //SSP MISO
	LPC_IOCON->PIO2_3 &= ~0x07;
	LPC_IOCON->PIO2_3 |= 0x02;                //SSP MOSI
	LPC_IOCON->PIO2_1 &= ~0x07;
	LPC_IOCON->PIO2_1 |= 0x02;                //SSP CLK
	LPC_IOCON->PIO2_0 &= ~0x07;
	LPC_IOCON->PIO2_0 |= 0x02;                //SSP SSEL
	LPC_IOCON->PIO3_0 &= ~0x07;				// ssp int
	GPIOSetDir(3, 0, 1);
	GPIOSetValue(3, 0, 0);
	// Set DSS data to 8-bit, Frame format SPI, CPOL = 0, CPHA = 0, and SCR is 12
	LPC_SSP1->CR0 = 0x0707;
	// SSPCPSR clock prescale register, master mode, minimum divisor is 0x02
	LPC_SSP1->CPSR = 0x2;
	for ( i = 0; i < FIFOSIZE; i++ ){
		Dummy = LPC_SSP1->DR;                    //clear the RxFIFO
	}
	//slave, SSP Enabled
	if ( LPC_SSP1->CR1 & SSPCR1_SSE ){
	/* The slave bit can't be set until SSE bit is zero. */
		LPC_SSP1->CR1 &= ~SSPCR1_SSE;
	}
	LPC_SSP1->CR1 = SSPCR1_MS;                //Enable slave bit first
	LPC_SSP1->CR1 |= SSPCR1_SSE;            //Enable SSP
	LPC_SSP1->IMSC = (SSPIMSC_RTIM | SSPIMSC_RXIM | SSPIMSC_RORIM);			/* Enable RxFifo half full and rx timeout interrupt */
	NVIC_EnableIRQ(SSP1_IRQn);
	for(i = 0; i < 100; i++){
		rx.buf[i] = 0x00;
		tx.buf[i] = 0x00;
	}
	rx.len = 0xff;
	rx.ready = 0;
	rx.cursor = rx.buf;
	rx.start = rx.buf;
	tx.len = 0;
	tx.ready = 0;
	tx.cursor = tx.buf;
	tx.start = tx.buf;
	ssp_callback = (void*)0;
}

void _int(){
	GPIOSetValue(3, 0, 1);
//	__int = 1;
//	int_timer = count_1khz();
}

void spisl_send(uint8_t *buf, uint8_t len){
	uint8_t crc = 0;
	uint8_t b;
	if(tx.len){
		printf("SPI TX Busy\r\n");
		if(!GPIOGetValue(3, 0)){
			_int();
		}
		return;
	}
//	printf("SPI TX Start: ");
	tx.len = len;
	memcpy(tx.buf+3, buf, len);
	tx.buf[0] = 0xBE;
	tx.buf[1] = 0xEF;
	tx.buf[2] = len;
	for(int i = 3; i < len+3; i++){
		crc ^= tx.buf[i];
	}
	tx.buf[tx.len+3] = crc;
	tx.cursor = tx.buf;
//	while(LPC_SSP1->SR & SSPSR_TNF && (tx.cursor - tx.buf < tx.len + 3 + 1)){
//		b = *tx.cursor++;
//		LPC_SSP1->DR = b;
//		printf("0x%02x ", b);
//	}
//	printf("\r\n");
	int_send();
	LPC_SSP1->IMSC |= SSPIMSC_TXIM;
	_int();
}

void spisl_set_callback(int (*callb)(uint8_t*, uint8_t)){
	ssp_callback = callb;
}

void spisl_poll(){
//	static uint32_t timer = 0;
//	static uint8_t b[] = {0xDE, 0xAD, 0xDE, 0xAD, 0xAA, 0x55, 0x00, 0x01};
}

void int_recv(){
	static int total = 0;
	static int success = 0;
	static int crcerr = 0;
	static int timeouterr = 0;
	static uint32_t timeout = 0;
	uint8_t i, b, crc;
	while(LPC_SSP1->SR & SSPSR_RNE){
		while((LPC_SSP1->SR & (SSPSR_BSY|SSPSR_TFE)) == (SSPSR_BSY|SSPSR_TFE));
		*rx.cursor++ = LPC_SSP1->DR;
		timeout = count_1khz();
//		printf("0x%02x ", *(rx.cursor-1));
		if(rx.cursor - rx.buf == 1 && rx.buf[0] != 0xBE){
			rx.cursor = rx.buf;
//			printf("!BE\r\n");
		}
		if(rx.cursor - rx.buf == 2 && rx.buf[1] != 0xEF){
			rx.cursor = rx.buf;
//			printf("!EF\r\n");
		}
		if(rx.cursor - rx.buf == 3){
			rx.len = rx.buf[2];
//			printf("OK ");
		}
	}
	if(rx.len != 0xff && count_1khz() - timeout > 20){
//		printf("Timeout\r\n");
		total++;
		timeouterr++;
		memset(rx.buf, 0x00, 100);
		rx.cursor = rx.buf;
		rx.len = 0xff;
	}
	if(rx.cursor - rx.buf >= rx.len + 3 + 1){
		crc = 0;
		for(i = 0; i < rx.len; i++){
			crc ^= rx.buf[i+3];
		}
		if(crc != rx.buf[rx.len+3]){
//			printf("CRC error, need: 0x%02x got: 0x%02x\r\n", crc, rx.buf[rx.len+3]);
			total++;
			crcerr++;
			memset(rx.buf, 0x00, 100);
			rx.cursor = rx.buf;
			rx.len = 0xff;
		}else if(ssp_callback){
			success++;
			total++;
//			printf("\x1B[2J");
//			printf("Total:\t%d\r\n", total);
//			printf("Fail:\t%d\tCRC: %d\tTimeout: %d\r\n", timeouterr+crcerr, crcerr, timeouterr);
//			printf("Success:\t%d\r\n", success);
			ssp_callback(rx.buf+3, rx.len);
			memset(rx.buf, 0x00, 100);
			rx.cursor = rx.buf;
			rx.len = 0xff;
		}
	}
}

void int_send(){
	uint8_t b;
//	printf("SPI TX: ");
	while(LPC_SSP1->SR & SSPSR_TNF && (tx.cursor - tx.buf < tx.len + 3 + 1)){
		b = *tx.cursor++;
		LPC_SSP1->DR = b;
//		printf("0x%02x ", b);
	}
//	printf("\r\n");
	if(tx.cursor - tx.buf >= tx.len + 3 + 1){
//		printf("TX END\r\n");
//		LPC_SSP1->IMSC &= ~(SSPIMSC_TXIM);
		memset(tx.buf, 0x00, 100);
		tx.cursor = tx.buf;
		tx.len = 0;
	}
}

void spisl_int(){
//	printf("SPISL INT\r\n");
	GPIOSetValue(3, 0, 0);
	if(!tx.len){
		LPC_SSP1->IMSC &= ~(SSPIMSC_TXIM);
		if(LPC_SSP1->MIS & SSPMIS_RORMIS){
//			printf("buffer overflow\r\n");
			while(LPC_SSP1->SR & SSPSR_RNE){
				uint8_t dummy = LPC_SSP1->DR;
//				printf("0x%02x ", dummy);
			}
//			printf("\r\n");
			LPC_SSP1->ICR |= 0x01;
			return;
		}
		if(LPC_SSP1->MIS & (SSPMIS_RTMIS|SSPMIS_RXMIS)){
			int_recv();
			LPC_SSP1->ICR |= 0x02;
		}
	}else{
		if(LPC_SSP1->MIS & SSPMIS_TXMIS){
			int_send();
		}
		while(LPC_SSP1->SR & SSPSR_RNE){
			uint8_t dummy = LPC_SSP1->DR;
		}
	}
}
