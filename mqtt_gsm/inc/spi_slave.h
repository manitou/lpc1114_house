/*
 * spi_slave.h
 *
 *  Created on: 07.01.2015
 *      Author: Admin
 */

#ifndef SPI_SLAVE_H_
#define SPI_SLAVE_H_

#include <stdint.h>

#ifndef __SSP_COMMON__
#define __SSP_COMMON__
#define FIFOSIZE		8
/* SSP Status register */
#define SSPSR_TFE       (0x1<<0)
#define SSPSR_TNF       (0x1<<1)
#define SSPSR_RNE       (0x1<<2)
#define SSPSR_RFF       (0x1<<3)
#define SSPSR_BSY       (0x1<<4)

/* SSP CR0 register */
#define SSPCR0_DSS      (0x1<<0)
#define SSPCR0_FRF      (0x1<<4)
#define SSPCR0_SPO      (0x1<<6)
#define SSPCR0_SPH      (0x1<<7)
#define SSPCR0_SCR      (0x1<<8)

/* SSP CR1 register */
#define SSPCR1_LBM      (0x1<<0)
#define SSPCR1_SSE      (0x1<<1)
#define SSPCR1_MS       (0x1<<2)
#define SSPCR1_SOD      (0x1<<3)

/* SSP Interrupt Mask Set/Clear register */
#define SSPIMSC_RORIM   (0x1<<0)
#define SSPIMSC_RTIM    (0x1<<1)
#define SSPIMSC_RXIM    (0x1<<2)
#define SSPIMSC_TXIM    (0x1<<3)

/* SSP0 Interrupt Status register */
#define SSPRIS_RORRIS   (0x1<<0)
#define SSPRIS_RTRIS    (0x1<<1)
#define SSPRIS_RXRIS    (0x1<<2)
#define SSPRIS_TXRIS    (0x1<<3)

/* SSP0 Masked Interrupt register */
#define SSPMIS_RORMIS   (0x1<<0)
#define SSPMIS_RTMIS    (0x1<<1)
#define SSPMIS_RXMIS    (0x1<<2)
#define SSPMIS_TXMIS    (0x1<<3)

/* SSP0 Interrupt clear register */
#define SSPICR_RORIC    (0x1<<0)
#define SSPICR_RTIC     (0x1<<1)
#endif /* SSP_COMMON */

typedef struct{
	uint8_t buf[100];
	uint8_t len, *cursor, *start;
	uint8_t ready;
}spi_pipe_t;

extern spi_pipe_t rx, tx;
extern int (*ssp_callback)(uint8_t *buf, uint8_t len);

void spisl_init(void);
void spisl_send(uint8_t *buf, uint8_t len);
void spisl_set_callback(int (*)(uint8_t*, uint8_t));
void spisl_poll(void);
void spisl_int(void);

#endif /* SPI_SLAVE_H_ */
