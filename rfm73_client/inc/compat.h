/*
 * nxp_compat.h
 *
 *  Created on: 07.12.2013
 *      Author: Admin
 */

#ifndef NXP_COMPAT_H_
#define NXP_COMPAT_H_
#include <stdint.h>

extern void rfm_int(void);

void _wait(uint8_t _10msec);
void _spi_tx(uint8_t data);
uint8_t _spi_rx(void);
void _radio_enable(void);
void _radio_disable(void);
void _rfm_select(void);
void _rfm_release(void);
void _compat_init(void);

#define debug printf



#endif /* NXP_COMPAT_H_ */
