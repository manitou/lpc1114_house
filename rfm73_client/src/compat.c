/*
 * nxp_compat.c
 *
 *  Created on: 07.12.2013
 *      Author: Admin
 */

#include "LPC11xx.h"
#include "spi.h"
#include "printf.h"
#include "compat.h"
#include "rfm.h"

void _wait(uint8_t _10msec){
}

void _spi_tx(uint8_t data){
	nrfspi_txrx(data);
}

uint8_t _spi_rx(){
	return nrfspi_txrx(0xff);
}

void _radio_enable(){
	nrf_start();
}

void _radio_disable(){
	nrf_stop();
}

void _rfm_select(){
	nrfspi_select();
}

void _rfm_release(){
	nrfspi_release();
}

void _compat_init(){
	nrfspi_init();
	//LPC_IOCON->PIO1_9 &= ~0x07;
	//LPC_GPIO1->DIR &= ~(1<<9);
	//LPC_GPIO1->IS &= ~(1<<9);
	//LPC_GPIO1->IBE &= ~(1<<9);
	//LPC_GPIO1->IEV &= ~(1<<9);
	//NVIC_EnableIRQ(EINT1_IRQn);
}
