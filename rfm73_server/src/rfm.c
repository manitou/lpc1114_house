/*
 * nrf.c
 *
 *  Created on: Nov 8, 2013
 *      Author: manitou
 */

#include <string.h>
#include "LPC11xx.h"
#include "rfm.h"
#include "RFM73.h"
#include "printf.h"
#include "counter.h"


#define PAYLOAD_SIZE 32

#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X, Y) ((X) > (Y) ? (X) : (Y))
//#define NULL (void*)0

VOLATILE rx_packet_t rx_packet;
VOLATILE void (*rx_callb)(rx_packet_t *packet);
VOLATILE void (*tx_callb)(uint8_t result);

uint8_t prev_status;
uint8_t tx_done;

//------------MAGIC
///Banks1 registers which must be writen
//0-8;	Byte(MSB->LSB), Bit(MSB->LSB)
uint8_t Reg00[]={0x40,0x4B,0x01,0xE2};
uint8_t Reg01[]={0xC0,0x4B,0x00,0x00};
uint8_t Reg02[]={0xD0,0xFC,0x8C,0x02};
uint8_t Reg03[]={0x99,0x00,0x39,0x41};
uint8_t Reg04[]={0xb9,0x96,0x82,0x1B}; // {0xD9,0xB6,0x82,0x1B};
uint8_t Reg05[]={0x28,0x02,0x7F,0xA6}; // {0x24,0x06,0x7F,0xA6};
//12-14;	Byte(LSB->MSB), Bit(MSB->LSB)
uint8_t Reg0C[]={0x00,0x12,0x73,0x00}; // {0x00,0x12,0x73,0x05};
uint8_t Reg0D[]={0x46,0xB4,0x80,0x00}; // {0x36,0xB4,0x80,0x00};
uint8_t Reg0E[]={0x41, 0x20, 0x08, 0x04, 0x81, 0x20, 0xCF, 0xF7, 0xFE, 0xFF, 0xFF}; // {0x41,0x10,0x04,0x82,0x20,0x08,0x08,0xF2,0x7D,0xEF,0xFF};
//-----------MAGIC END


uint8_t pipe_addresses[6] = {RX_ADDR_P0, RX_ADDR_P1, RX_ADDR_P2, RX_ADDR_P3, RX_ADDR_P4, RX_ADDR_P5};
uint8_t pipe_payloads[6] = {RX_PW_P0, RX_PW_P1, RX_PW_P2, RX_PW_P3, RX_PW_P4, RX_PW_P5};

void rfm_readn(uint8_t reg, uint8_t *buf, uint8_t len){
	_rfm_select();
	_spi_tx(R_REGISTER | (reg & REGISTER_MASK));
	while(len--){
		*buf = _spi_rx();
		buf++;
	}
	_rfm_release();
}

uint8_t rfm_read(uint8_t reg){
	uint8_t ret = 0;
	_rfm_select();
	_spi_tx(R_REGISTER | (reg & REGISTER_MASK));
	ret = _spi_rx();
	_rfm_release();
	return ret;
}

void rfm_writen(uint8_t reg, uint8_t *buf, uint8_t len){
	_rfm_select();
	_spi_tx(W_REGISTER | (reg & REGISTER_MASK));
	while(len--){
		_spi_tx(*buf++);
	}
	_rfm_release();
}

void rfm_write(uint8_t reg, uint8_t data){
	_rfm_select();
	_spi_tx(W_REGISTER | (reg & REGISTER_MASK));
	_spi_tx(data);
	_rfm_release();
//	uint8_t test = rfm_read(reg);
//	while(test != data && reg != 7){
//		led_blink(LED2);
//	}

}

static void rfm_switch_banks(uint8_t bank){
	uint8_t b = rfm_read(STATUS) >> 7;
	if(bank != b){
		_rfm_select();
		_spi_tx(ACTIVATE);
		_spi_tx(0x53);
		_rfm_release();
	}
}

void rfm_write_payload(uint8_t *buf, uint8_t len){
	uint8_t data_len = MIN(PAYLOAD_SIZE, len);
	uint8_t blank_len = MAX(0, PAYLOAD_SIZE-len);
	_rfm_select();
	_spi_tx(W_TX_PAYLOAD);
	while(data_len--){
		_spi_tx(*buf++);
	}
	while(blank_len--){
		_spi_tx(0xff);
	}
	_rfm_release();
}

void rfm_write_payload_noack(uint8_t *buf, uint8_t len){
	uint8_t data_len = MIN(PAYLOAD_SIZE, len);
	uint8_t blank_len = MAX(0, PAYLOAD_SIZE-len);
	_rfm_select();
	_spi_tx(W_TX_PAYLOAD_NOACK);
	while(data_len--){
		_spi_tx(*buf++);
	}
	while(blank_len--){
		_spi_tx(0xff);
	}
	_rfm_release();
}

void rfm_read_payload(uint8_t *buf, uint8_t len){
	uint8_t data_len = MIN(PAYLOAD_SIZE, len);
	uint8_t blank_len = MAX(0, PAYLOAD_SIZE-len);
	_rfm_select();
	_spi_tx(R_RX_PAYLOAD);
	while(data_len--){
		*buf = _spi_rx();
		buf++;
	}
	while(blank_len--){
		_spi_rx();
	}
	_rfm_release();
}

void rfm_flush_tx(){
	_rfm_select();
	_spi_tx(FLUSH_TX);
	_rfm_release();
}

void rfm_flush_rx(){
	_rfm_select();
	_spi_tx(FLUSH_RX);
	_rfm_release();
}

void rfm_set_channel(uint8_t ch){
	rfm_write(RF_CH, ch);
}

void rfm_set_power(uint8_t pwr){
	uint8_t setup = rfm_read(RF_SETUP);
	setup &= ~((1<<RF_PWR_LOW)|(1<<RF_PWR_HIGH));
	setup |= (pwr&0x3)<<1;
	rfm_write(RF_SETUP, setup);
}

void rfm_set_speed(uint8_t spd){
	uint8_t setup = rfm_read(RF_SETUP);
	setup &= ~(1<<RF_DR);
	setup |= (spd&1)<<RF_DR;
	rfm_write(RF_SETUP, setup);
}

void rfm_set_crc(uint8_t enable, uint8_t len){
	uint8_t config = rfm_read(CONFIG);
	config &= ~((1 << EN_CRC)|(1 << CRCO));
	config |= ((enable&1)<<EN_CRC) | (((len-1)&1)<<CRCO);
	rfm_write(CONFIG, config);
}

uint8_t rfm_get_status(){
	uint8_t status;
	_rfm_select();
	status = _spi_rx();
	_rfm_release();
	return status;
	//more complicated version
	//return nrf_read(STATUS);
}

void rfm_activate(){
	if(!rfm_read(FEATURE)){
		_rfm_select();
		_spi_tx(ACTIVATE);
		_spi_tx(0x73);
		_rfm_release();
	}
}

void VOLATILE rfm_int(){
	//VOLATILE uint16_t counter = 300;
	uint8_t status = rfm_get_status();
	if(status>>7){
		rfm_switch_banks(0);
	}
	if(!(status>>4) && (status&0x0e) == 0x0e)
		return;
	//debug("INT ");
	//debug("Status: %x\r\n", status);
	if(status & (1 << 5)){
		// TX done
		rfm_write(STATUS, (1 << 5));
		if(tx_callb != NULL)
			tx_callb(1);
		if(rfm_read(FIFO_STATUS) & (1 << TX_EMPTY)){
			//debug("Transmission done.\r\n");
			//_radio_disable();
			//rfm_write(CONFIG, rfm_read(CONFIG) | (1<<PRIM_RX));
			//_radio_enable();
			//while(counter--);
			tx_done = 1;
		}else{
			//rfm_send();
		}
	}
	if(status & (1 << 4)){
		// TX fail
		rfm_write(STATUS, (1 << 4));
		if(tx_callb != NULL)
			tx_callb(0);
		tx_done = 1;
		rfm_flush_tx();
//		rfm_write(CONFIG, rfm_read(CONFIG) | (1<<PRIM_RX));
//		_radio_enable();
//		while(counter--);
	}
	if(status & (1 << 6) || (status&0x0e) != 0x0e){
		// got RX
		rfm_write(STATUS, (1 << 6));
		rx_packet.pipe = (status>>1)&0b111;
		rfm_read_payload(rx_packet.packet, 32);
		rfm_flush_rx();
		if(rx_callb != NULL)
			rx_callb(&rx_packet);
	}
}

void rfm_init(){
	_compat_init();
	_radio_disable();
	_rfm_select();
	rfm_powerup();
	_wait(5);

	//-----------MAGIC START
	//extra weird magic starts
	rfm_switch_banks(1);
	rfm_writen(A_00, Reg00, 4);
	rfm_writen(A_01, Reg01, 4);
	rfm_writen(A_02, Reg02, 4);
	rfm_writen(A_03, Reg03, 4);
	rfm_writen(A_04, Reg04, 4);
	rfm_writen(A_05, Reg05, 4);
	rfm_writen(A_0c, Reg0C, 4);
	rfm_writen(A_0d, Reg0D, 4);
	rfm_writen(A_0e, Reg0E, 11);
	//-------------MAGIC END
	rfm_switch_banks(0);
	rfm_write(CONFIG, 0x0f);
	rfm_write(EN_AA, 0x0);
	rfm_write(EN_RXADDR, 0x01);
	//pipes set here
	rfm_write(SETUP_AW, 0x01); /* set 3 byte addresses */
	rfm_write(SETUP_RETR, 0); /* no retry*/
	//rfm_write(SETUP_RETR, (0b0010<<ARD)|(0b1111<<ARC)); /* 750us retransmit delay and 15 retries */
	//channel set here
	rfm_write(RF_SETUP, 0x0f); //250Kbps, 5 dBm
	//rfm_write(RF_SETUP, 0x27); //2Mbps, 5 dBm
	//rfm_write(RX_PW_P0, 32);
	rfm_write(STATUS, (1 << RX_DR)|(1 << TX_DS)|(1 << MAX_RT)); /* clean interrupt flags */
	tx_callb = NULL;
	rx_callb = NULL;
	rfm_flush_rx();
	rfm_flush_tx();
	rfm_write(STATUS, (1 << RX_DR)|(1 << TX_DS)|(1 << MAX_RT)); /* clean interrupt flags */
	prev_status = rfm_get_status();
	tx_done = 1;
	//rfm_powerup();
	//_radio_disable();
	//rfm_write(CONFIG, rfm_read(CONFIG) | (1<<PRIM_RX));
	//_radio_enable();
	//_interrupt(1);
}

void rfm_listen(){
	VOLATILE uint16_t counter = 50;
	_radio_disable();
	rfm_write(CONFIG, rfm_read(CONFIG)|(1 << PRIM_RX));
	rfm_write(STATUS, rfm_read(STATUS)|(1 << RX_DR)|(1 << TX_DS)|(1 << MAX_RT));
	rfm_flush_rx();
	rfm_flush_tx();
	_radio_enable();
	while(counter--);
}

void rfm_nolisten(){
	_radio_disable();
	rfm_flush_rx();
	rfm_flush_tx();
}

void rfm_powerdown(){
	rfm_nolisten();
	rfm_write(CONFIG, rfm_read(CONFIG) & ~(1<<PWR_UP));
}

void rfm_powerup(){
	VOLATILE uint16_t i = 300;
	rfm_write(CONFIG, rfm_read(CONFIG)|(1<<PWR_UP)|(1 << PRIM_RX));
	_radio_enable();
	while(i--);
	_radio_disable();
	rfm_listen();
}

void rfm_set_rx_pipes(uint8_t num){
	uint8_t temp;
	uint8_t mask = ~(0xff << num);
	rfm_write(EN_RXADDR, mask);
	do{
		temp = rfm_read(EN_RXADDR);
	}while(temp != mask);
//	rfm_write(EN_AA, mask);
//	do{
//		temp = rfm_read(EN_AA);
//	}while(temp != mask);
	while(num--){
		rfm_write(pipe_payloads[num], 32);
	}
}

void rfm_set_pipe_addr(uint8_t pipe, uint8_t *addr){
	uint8_t buf[5];
	if(pipe > 1){
		rfm_writen(pipe_addresses[pipe], addr, 1);
	}else{
		rfm_writen(pipe_addresses[pipe], addr, 3);
	}
	rfm_readn(pipe_addresses[pipe], buf, 5);
	//_rfm_release();
}

void rfm_set_tx_addr(uint8_t *addr){
	uint8_t buf[5];
	rfm_writen(TX_ADDR, addr, 3);
	rfm_readn(TX_ADDR, buf, 3);
	//_rfm_release();
}

void rfm_send(){
	//VOLATILE uint8_t i = 50;
	//FIFO must already have packet
	if(rfm_read(FIFO_STATUS) & (1 << TX_EMPTY)){
		return;
	}
	tx_done = 0;
	_radio_disable();
	rfm_write(CONFIG, rfm_read(CONFIG) & ~(1<<PRIM_RX));
	_radio_enable();
	while(!tx_done); // || i--
	rfm_listen();
	//_radio_disable();

}

//interrupt only, use only for ACK
void rfm_fast_send(uint8_t *buf, uint8_t len){
	VOLATILE uint16_t i = 400;
	rfm_write_payload(buf, len);
	//while(rfm_read(FIFO_STATUS) & (1 << TX_EMPTY));
	_radio_disable();
	rfm_write(CONFIG, rfm_read(CONFIG) & ~(1<<PRIM_RX));
	_radio_enable();
	while(rfm_get_status() & 0x30);
	while(i--);
		__NOP(); //magic delay, MUST BE >= 200
	rfm_listen();
}

uint8_t rfm_rts(){
	return rfm_read(FIFO_STATUS) & (1 << TX_EMPTY);
}

void rfm_set_tx_callback(void (*callb)(uint8_t)){
	tx_callb = callb;
}

void rfm_set_rx_callback(void (*callb)(rx_packet_t*)){
	rx_callb = callb;
}

void rfm_reset(){
	VOLATILE uint16_t i = 600;
	//rfm_flush_tx();
	//rfm_flush_rx();
	rfm_powerdown();
	while(i--);
	rfm_powerup();
	//rfm_listen();
}

/* TODO: add callbacks to nrf_int(), add PRIM_TX control with 4ms window
 * add low level RF control (endpoint and HUB different code)
 */
