/*
 * rfs_rfm.c
 *
 *  Created on: 18.07.2014
 *      Author: Admin
 */
#include <string.h>
#include "LPC11xx.h"
#include "rfm.h"
#include "printf.h"
#include "gpio.h"
#include "server_rfm.h"
#include "leds.h"
#include "pwr.h"

//SOME PROBLEMS WITH VOLATIVITY IN RFM PART
//USING VOLATILE MACRO FROM LPC11xx.h

// RFM part
VOLATILE uint8_t buffer[40];
VOLATILE uint8_t len;
VOLATILE uint16_t to_resolve;

//MAC part
VOLATILE uint8_t self[3];

VOLATILE bind_table_t bind_table[12];

static uint8_t is_in_table(uint16_t addr){
	for(uint8_t i = 0; i < 12; i++){
		if(addr == bind_table[i].addr)
			return 1;
	}
	return 0;
}

static uint8_t* add_seq(uint16_t addr){
	uint8_t i;
	for(i = 0; i < 12; i++){
		if(bind_table[i].addr == 0)
			break;
	}
	bind_table[i].addr = addr;
	return &(bind_table[i].seq);
}

static uint8_t* get_seq(uint16_t addr){
	for(uint8_t i = 0; i < 12; i++){
		if(addr == bind_table[i].addr){
			return &(bind_table[i].seq);
		}
	}
	return 0;
}

void recv_callback(rx_packet_t *packet){
	uint16_t addr = 0;
	led_blink(LED2);
	printf("Incoming: pipe %u len %u ", packet->pipe, packet->packet[0]);
	if(packet->packet[0] > 32)
		return;
	addr = packet->packet[1];
	addr <<= 8;
	addr |= packet->packet[2];
	if(packet->pipe == 0 && packet->packet[0] == 2){
		//ask packet
		if(is_in_table(addr)){
			//make ask ack
			printf("%u ask ack\r\n", addr);
			packet->packet[0] = 5;
			memcpy(packet->packet+3, self, 3);
			rfm_set_tx_addr((VOLATILE uint8_t[]){0xe7, 0xe7, 0xe7});
			rfm_fast_send(packet->packet, 6);
			//rfm_set_tx_addr(self);
			to_resolve = 0;
		}else{
			to_resolve = addr;
			printf("To resolve %u\r\n", to_resolve);
		}
		return;
	}
	if(packet->pipe == 1){
		printf(" seq %u ", packet->packet[3]);
		//send ack
		packet->packet[0] = 2;
		rfm_set_tx_addr(self);
		rfm_fast_send(packet->packet, 3);
		//check for retransmission
		uint8_t *seq = get_seq(addr);
		if(seq == NULL)
			seq = add_seq(addr); //on the fly add address to bind table if it already talks on our channel
		//regular data
		if(packet->packet[3] <= *seq){ //check if our packet is new, not retransmission
			//new transmission
			printf("reg data");
			memcpy(buffer, packet->packet, 32);
			len = 32;
		}
		*seq = packet->packet[3];
		printf("\r\n");
		//led_blink(LED2);
		return;
	}
}

void rfs_init(uint16_t _self){
	memset(buffer, 0x00, 40);
	len = 0;
	to_resolve = 0;
	self[0] = _self>>8;
	self[1] = _self&0xff;
	self[2] = 0x01;
	for(uint8_t i = 0; i < 12; i++){
		bind_table[i].addr = 0;
		bind_table[i].seq = 0;
	}
	LPC_IOCON->PIO1_9 &= ~0x07;
	GPIOSetDir(1, 9, 0);
	GPIOSetValue(1, 9, 0);
	GPIOIntDisable(1, 9);
	GPIOSetInterrupt(1, 9, 1, 0, 0);
	NVIC_EnableIRQ(EINT1_IRQn);
	rfm_init();
	//rfm_set_tx_addr((uint8_t[]){0x01, 0x02, 0x03});
	rfm_set_rx_pipes(2);
	rfm_set_pipe_addr(1, self);
	rfm_set_rx_callback(recv_callback);
	rfm_set_channel(73);
	GPIOIntEnable(1, 9);
	led_on(LED2);
	rfm_listen();
}

uint8_t rfs_send(uint16_t addr, uint8_t *data, uint8_t len){
	static uint8_t packet[32];
	packet[0] = len+2;
	packet[1] = addr>>8;
	packet[2] = addr&0xff;
	memcpy(packet+3, data, len);
	len += 3;
	rfm_write_payload(packet, len);
	rfm_send();
	led_blink(LED2);
	return 1;
}

void rfs_ask_ack(uint16_t addr){
//this is in past, now we only add addresses to table
//	static uint8_t packet[10];
//	//make packet
//	packet[0] = 5;
//	packet[1] = addr>>8;
//	packet[2] = addr&0xff;
//	memcpy(packet+3, self, 3);
//	//switch to broadcast
//	rfm_set_tx_addr((uint8_t[]){0xe7, 0xe7, 0xe7});
//	//send
//	//rfm_write_payload(packet, 6);
//	//rfm_send();
//	rfm_fast_send(packet, 6);
//	//switch back
//	rfm_set_tx_addr(self);
	//uint8_t i;
	add_seq(addr);
}

uint16_t rfs_need_resolve(){
	uint16_t ret = to_resolve;
	to_resolve = 0;
	return ret;
}

uint8_t rfs_recv(uint8_t *data, uint8_t timeout){
	uint8_t ret = 0;
	if(timeout){
		//waiting for packet
		//asm_wait_nonblock(timeout, &_timeout);
		while(!len && timeout--)
			asm_wait(1);
		if(!timeout){
			//nothing to receive
			return 0;
		}
	}
	if(len){
		//returnning callback packet
		memcpy(data, buffer, len);
		ret = len;
		len = 0;
		return ret;
	}
	return ret;
}

void rfs_refresh_rfm(){
//	//switch to broadcast
//	rfm_set_tx_addr((uint8_t[]){0xe7, 0xe7, 0xe7});
//	//send
//	rfm_write_payload((uint8_t[]){0xff, 0x22, 0x33, 0x44}, 4);
//	rfm_send();
//	//rfm_fast_send(packet, 6);
//	//switch back
//	rfm_set_tx_addr(self);
	rfm_reset();
}

void rfs_config(){
	printf("RFM Config: ");
	for(uint8_t i = 0; i < 24; i++){
		printf("0x%02x ", rfm_read(i));
	}
	printf("\r\n");
}
