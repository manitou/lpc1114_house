/*
 * simple_rfm.h
 *
 *  Created on: 18.07.2014
 *      Author: Admin
 */

#ifndef RFM73_SERVER_H_
#define RFM73_SERVER_H_
#include <stdint.h>

/* RFM Server Library
 * Simplifies communication between devices via RF channel. This is
 * server part of library and have to be set only on one device in group.
 * All communication between listener (L) and node (N) are made in such way:
 * On start:
 *  - N sends broadcast ask for bind
 *  - L checks if this N belongs to him (No - paket ignored, Yes - N address
 *   	added to bind table)
 *  - N sends broadcast ask for bind
 *  - L answers N with private channel address
 *  - N changes channel to private
 * On usual transmission:
 *  - N sends data to L
 *  - L answers with data or ACK
 * Also server part have to be always listening, so it cannot be
 * stopped anyway. Server can initiate send only to not sleeping nodes
 * otherwise it's not guaranteed that node receive data.
 * Simple working instruction:
 *  - init
 *  - in loop:
 *  	- check for pending packets (rfs_recv) -> answer (rfs_send)
 * 		- check for pending bindings (rfs_need_resolve) -> bind (rfs_ask_ack)
 * 	- send data
 */

typedef struct{
	uint16_t addr;
	uint8_t seq;
}bind_table_t;

// RFM Server init function. Sets inner address of server. If you have more
// than one server in near field, be sure they have different addresses.
// Args:
//	_self		- self server address
void rfs_init(uint16_t _self);

// Sends data to specified address. Notice, that most nodes are sleeping nodes,
// and can only receive answers, not standalone packets. Be sure you're trying
// talk to awaken node.
// Args:
//	addr		- destination address (remote device ID)
//	data		- data to send
//	len			- length of data (<29 bytes)
// Returns: always 1. There's no ACK yet.
uint8_t rfs_send(uint16_t addr, uint8_t *data, uint8_t len);

// Binds node to this listener.
// Args:
//	addr 		- address of node to bind (remote device ID)
void rfs_ask_ack(uint16_t addr);

// Check for incoming data.
// Args:
//	data 		- place to put incoming data
//	timeout		- time for waiting data if no data is pending (set 0 to disable wait)
// Returns: amount of bytes readden
uint8_t rfs_recv(uint8_t *data, uint8_t timeout);

// Checks if any device is waiting for bind
// Returns: remote device ID to check for bind, 0 - if no device are waiting
uint16_t rfs_need_resolve(void);

// Service function. Resets RFM module. Use only if you have RF freezes.
void rfs_refresh_rfm(void);

//Config testing function. Do not use. For debug only.
void rfs_config(void);

//RFM Interrupt function, for use in irc_handlers.c file. Don not call directly.
extern void rfm_int(void);

// RFM module test function. Do not use. For debug only.
extern uint8_t rfm_get_status(void);
#endif /* RFM73_SERVER_H_ */
