################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/compat.c \
../src/rfm.c \
../src/server_rfm.c 

OBJS += \
./src/compat.o \
./src/rfm.o \
./src/server_rfm.o 

C_DEPS += \
./src/compat.d \
./src/rfm.d \
./src/server_rfm.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M0 -D__LPC11XX__ -I"C:\LPC1114_GIT\core" -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx\inc" -I"C:\LPC1114_GIT\rfm73_server\inc" -I"C:\LPC1114_GIT\rfm73_server" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -std=c99 -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


