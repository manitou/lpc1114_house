/*
 * nrf.h
 *
 *  Created on: Nov 8, 2013
 *      Author: manitou
 */

#ifndef RFM_H_
#define RFM_H_
#include <stdint.h>

/* callback functions:
 * TX callback: void tx_callb(uint8_t result);
 * RX callback: void rx_callb(rx_packet_t *packet);
 *  warning: upper level have to manually set rx_packet->pipe to 0b111 to confirm packet receive
 */

#ifdef AVR
#include "avr_compat.h"
#endif
#ifdef __LPC11XX__
#include "compat.h"
#endif

typedef struct{
	uint8_t pipe;
	uint8_t packet[32];
}rx_packet_t;

void rfm_readn(uint8_t reg, uint8_t *buf, uint8_t len);
uint8_t rfm_read(uint8_t reg);
void rfm_writen(uint8_t reg, uint8_t *buf, uint8_t len);
void rfm_write(uint8_t reg, uint8_t data);
void rfm_write_payload(uint8_t *buf, uint8_t len);
void rfm_write_payload_noack(uint8_t *buf, uint8_t len);
void rfm_read_payload(uint8_t *buf, uint8_t len);
void rfm_flush_tx(void);
void rfm_flush_rx(void);
void rfm_set_channel(uint8_t ch);
void rfm_set_power(uint8_t pwr);
void rfm_set_speed(uint8_t spd);
void rfm_set_crc(uint8_t enable, uint8_t len);
uint8_t rfm_get_status(void);
void rfm_activate(void);
void rfm_init(void);
void rfm_listen(void);
void rfm_nolisten(void);
void rfm_powerdown(void);
void rfm_powerup(void);
void rfm_set_rx_pipes(uint8_t num);
void rfm_set_pipe_addr(uint8_t pipe, uint8_t *addr);
void rfm_set_tx_addr(uint8_t *addr);
void rfm_send(void);
void rfm_fast_send(uint8_t *buf, uint8_t len);
uint8_t rfm_rts(void);
void rfm_set_rx_callback(void (*callb)(rx_packet_t*));
void rfm_set_tx_callback(void (*callb)(uint8_t));
void rfm_int(void);
void rfm_reset(void);

#endif /* RFM_H_ */
