/*
 * lan_utils.c
 *
 *  Created on: 13.06.2014
 *      Author: Admin
 */

#include "lan_utils.h"
#include "lan.h"
#include "printf.h"
#include "counter.h"

uint8_t ping_state = 0xff;

uint8_t lu_ping(uint32_t addr){
	uint8_t ret = 0;
	if(ping_state == 0xff){ //first call sends PING
		ping_state = 0;
		icmp_send(addr);
	}else{ 					//second call returns answer
		ret = ping_state;
		ping_state = 0xff;
	}
	return ret;				//have to wait between them for some time
}

void icmp_packet(eth_frame_t *frame){
	//ip_packet_t *ip = (void*)(frame->data);
//	if(ip->from_addr == ip_gateway){
//		uart_printf("ICMP REPLY: Gateway is UP\r\n");
//		gateway_ping = 0x01;
//	}
//	if(ip->from_addr == broker.ip){
//		uart_printf("ICMP REPLY: Server is UP\r\n");
//		server_ping = 0x01;
//	}
	printf("ICMP REPLY\r\n");
	ping_state = 1;
}

void lu_dhcp_reset(){
	printf("DHCP Reset\r\n");
	ip_mask = 0;
	if(dhcp_status == DHCP_ASSIGNED)
		dhcp_renew_time = count_1hz() + 2;
	else
		dhcp_retry_time = count_1hz() + 2;
}

