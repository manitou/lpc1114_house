/*
 *  libmqtt.c
 *  libmqtt
 *
 *  Created by Filipe Varela on 09/10/16.
 *  Copyright 2009 Caixa Mágica Software. All rights reserved.
 *
 */

#include <string.h>
#include <stdlib.h>
#include "libmqtt.h"

uint8_t mqtt_connect(uint8_t *packet, const char *clientid)
{
	
	// variable header
	uint8_t var_header[] = {0x00,0x06,0x4d,0x51,0x49,0x73,0x64,0x70,0x03,0x02,0x00,KEEPALIVE/500,0x00,strlen(clientid)};
	
	// fixed header: 2 bytes, big endian
	uint8_t fixed_header[] = {MQTTCONNECT,12+strlen(clientid)+2};
	
	//memset(packet,0,sizeof(packet));
	memcpy(packet,fixed_header,sizeof(fixed_header));
	memcpy(packet+sizeof(fixed_header),var_header,sizeof(var_header));
	memcpy(packet+sizeof(fixed_header)+sizeof(var_header),clientid,strlen(clientid));

	return sizeof(fixed_header)+sizeof(var_header)+strlen(clientid);
}

uint8_t mqtt_publish(uint8_t *packet, const char *topic, char *msg)
{
	uint8_t var_header[strlen(topic)+2];
	strcpy((char *)&var_header[2], topic);
	var_header[0] = 0;
	var_header[1] = strlen(topic);
	//var_header[sizeof(var_header)-1] = 0;
	
	uint8_t fixed_header[] = {MQTTPUBLISH,sizeof(var_header)+strlen(msg)};
	
	//memset(packet,0,sizeof(packet));
	memcpy(packet,fixed_header,sizeof(fixed_header));
	memcpy(packet+sizeof(fixed_header),var_header,sizeof(var_header));
	memcpy(packet+sizeof(fixed_header)+sizeof(var_header),msg,strlen(msg));

	return sizeof(fixed_header)+sizeof(var_header)+strlen(msg);
}

uint8_t mqtt_subscribe(uint8_t *packet, const char *topic)
{
	uint8_t var_header[] = {0,10};	
	uint8_t fixed_header[] = {MQTTSUBSCRIBE,sizeof(var_header)+strlen(topic)+3};
	
	// utf topic
	uint8_t utf_topic[strlen(topic)+3];
	strcpy((char *)&utf_topic[2], topic);

	utf_topic[0] = 0;
	utf_topic[1] = strlen(topic);
	utf_topic[sizeof(utf_topic)-1] = 0;
	
	//memset(packet,0,sizeof(packet));
	memcpy(packet,fixed_header,sizeof(fixed_header));
	memcpy(packet+sizeof(fixed_header),var_header,sizeof(var_header));
	memcpy(packet+sizeof(fixed_header)+sizeof(var_header),utf_topic,sizeof(utf_topic));
	
	return sizeof(var_header)+sizeof(fixed_header)+strlen(topic)+3;
}
