/*
 * mqtt_wrapper.c
 *
 *  Created on: 30.07.2014
 *      Author: Admin
 */

#include <string.h>
#include "LPC11xx.h"
#include "lan.h"
#include "lan_utils.h"
#include "libmqtt.h"
#include "loopbuffer.h"
#include "counter.h"
#include "printf.h"
#include "mqtt_wrapper.h"
#include "leds.h"

// CALLBACKS =======================================================
void _auth_callback();
void _sub_callback();
void _pub_callback(uint16_t len);
void _ping_callback();

// LAN PART ========================================================
void _lan_init(){
	lan_init();
}

uint8_t _lan_ready(){
	return lan_up();
}

void _lan_reset(){
	lu_dhcp_reset();
}

// TCP PART ========================================================
//vars
uint8_t _tcp_socket = 0xff;
eth_frame_t *_tcp_frame = NULL;
uint32_t _tcp_recv_time = 0;
uint32_t _tcp_send_time = 0;

//funcs
void _tcp_init(){
	_tcp_socket = 0xff;
	_tcp_frame = (eth_frame_t*)net_buf;
	_tcp_recv_time = 0;
	_tcp_send_time = 0;
}

uint8_t _tcp_ready(){
	return (_tcp_socket != 0xff) && \
			(tcp_get_status(_tcp_socket) == TCP_ESTABLISHED) && \
			(count_1hz() - _tcp_recv_time < 10);
}

void _tcp_connect(){
	static uint32_t gateway_ping_time = 0; //msecond timers
	static uint32_t server_ping_time = 0; //msecond timers
	static uint8_t gateway_ping = 0xff;
	static uint8_t server_ping = 0xff;
	if(_tcp_socket == 0xff){ //connection not started yet
		//gateway ping check
		if(gateway_ping == 0xff){
			if(count_1khz() - gateway_ping_time > 2000 || gateway_ping_time == 0){
				//do ping
				printf("TCP: Gateway ping send\r\n");
				gateway_ping = lu_ping(ip_gateway);
				gateway_ping_time = count_1khz();
			}
			return;
		}else if(gateway_ping == 0){
			if(count_1khz() - gateway_ping_time > 1000){
				//timeout exceeded - time to check our ping
				gateway_ping = lu_ping(ip_gateway);
				gateway_ping_time = count_1khz();
				if(!gateway_ping){
					//gateway not respond - reset DHCP
					printf("TCP: Gateway is DOWN\r\n");
					printf("TCP: LAN Reset\r\n");
					_lan_reset();
					gateway_ping = 0xff;
				}else{//on success ping we just proceed to next ping
					printf("TCP: Gateway is UP\r\n");
				}
			}
			return;
		}else{ //gateway ping success

		}
		//server ping check
		if(server_ping == 0xff){
			if(count_1khz() - server_ping_time > 2000 || server_ping_time == 0){
				//do ping
				printf("TCP: Server ping send\r\n");
				server_ping = lu_ping(SERVER);
				server_ping_time = count_1khz();
			}
			return;
		}else if(server_ping == 0){
			if(count_1khz() - server_ping_time > 1000){
				//timeout exceeded - time to check our ping
				server_ping = lu_ping(SERVER);
				server_ping_time = count_1khz();
				if(!server_ping){
					//server not responding, nothing to do here, only wait
					printf("TCP: Server is DOWN\r\n");
					server_ping = 0xff;
					//nothing special, but if we're stuck pinging server
					//it's right time to recheck gateway
					if(count_1khz() - gateway_ping_time > 10000){
						gateway_ping = 0xff;
					}
				}else{//on success we just going to next step - connection
					printf("TCP: Server is UP\r\n");
				}
			}
		}else{ //server ping success

		}
		//making connection
		if(gateway_ping == 1 && server_ping == 1){
			tcp_reset();
			uint16_t port = (count_1khz()%32000) + 32000;
			printf("TCP: Opening connection from port %u\r\n", port);
			_tcp_socket = tcp_open(SERVER, htons(1883), htons(port));
			if(_tcp_socket != 0xff){
				//socket started
				gateway_ping = 0xff;
				server_ping = 0xff;
				_tcp_send_time = count_1hz();
			}else{
				//some error in tcp
				printf("TCP: TCP Reset\r\n");
				tcp_reset();
				//and retry
			}
		}
	}else{ //connection already started
		tcp_status_code_t state = tcp_get_status(_tcp_socket);
		if(state == TCP_CLOSED){
			//connetion is closed somehow - return to connection
			printf("TCP: Connection closed\r\n");
			_tcp_socket = 0xff;
			return;
		}else if(state == TCP_ESTABLISHED){
			//desired state, nothing to do
			//all other will be done in callbacks
			printf("TCP: Connection established\r\n");
			return;
		}else{

		}
		//in all other states we just check for timeout and wait
		if(count_1hz() - _tcp_send_time > 10){
			//connection time exceeded - need to retry
			printf("TCP: Connection timeout reset\r\n");
			_tcp_socket = 0xff;
			return;
		}
	}
}

void _tcp_send(uint8_t *data, uint16_t len){
	ip_packet_t *ip = (void*)(_tcp_frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
	memcpy(tcp->data, data, len);
	tcp_send(_tcp_socket, _tcp_frame, len, TCP_OPTION_PUSH|TCP_OPTION_SEND);
	_tcp_send_time = count_1hz();
	led_blink(LED1);
}

void _tcp_reset(){
	_tcp_socket = 0xff;
	_tcp_send_time = 0;
	_tcp_recv_time = 0;
	//tcp_reset();
}

// MQTT AUTH PART ===================================================
//vars
uint8_t _auth_done = 0;
uint32_t _auth_time = 0; //msecond timer
//funcs
void _auth_init(){
	_auth_done = 0;
}

uint8_t _auth_ready(){
	return _auth_done;
}

void _auth_do(){
	if(count_1khz() - _auth_time > 2000 || _auth_time == 0){
		//making auth
		uint8_t packet[80] = {0};
		char id[] = CLIENT_ID;
		uint8_t packet_len = mqtt_connect(packet, id);
		printf("AUTH (%s): Send\r\n", id);
		_tcp_send(packet, packet_len);
		_auth_time = count_1khz();
	}
	//otherwise we waiting and rely on TCP timeouts
}

void _auth_callback(){
	ip_packet_t *ip = (void*)(_tcp_frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
	if((tcp->data[0]>>4) == 2){
		//got CONNACK
		if(tcp->data[3] == 0){
			//authorized
			printf("AUTH: Authorized\r\n");
			_auth_done = 1;
		}else{
			//error with code in tcp->data[3]
			printf("AUTH: Error authorizing (code %u)\r\n", tcp->data[3]);
		}
	}
}

void _auth_reset(){
	_auth_done = 0;
	_auth_time = 0;
}

// MQTT SUBSCRIBE PART ===============================================
//vars
uint8_t _sub_state = 0;
uint32_t _sub_time = 0;
mqtt_sub_t _sub_subs[5];
uint8_t _sub_subs_num = 0;
//TODO: make subscriptions system
// - add subs in runtime
//   - send new subscribed channel packet
// - send subs on start or on reconnect
// - process callbacks not only from SUBACK, but also from PUBLISH
//funcs
void _sub_init(){
	_sub_state = 0;
	_sub_time = 0;
}

uint8_t _sub_ready(){
	return (_sub_state == _sub_subs_num);
}

void _sub_do(){
	static uint8_t sub_sent = 0xff;
	if(_sub_state < _sub_subs_num){
		if(sub_sent < _sub_state || sub_sent == 0xff){
			//sending SUBSCRIBE
			uint8_t packet[80] = {0};
			uint8_t packet_len = mqtt_subscribe(packet, _sub_subs[_sub_state].topic);
			printf("SUB: Subscribe \"%s\"\r\n", _sub_subs[_sub_state].topic);
			_tcp_send(packet, packet_len);
			_sub_time = count_1hz();
			sub_sent = _sub_state;
			return;
		}
		if(count_1hz() - _sub_time > 5){
			//timeout exceeded need to retry
			printf("SUB: Timeout, retry\r\n");
			sub_sent = 0xff;
			return;
		}
	}else{
		sub_sent = 0xff;
	}
}

void _sub_callback(){
	ip_packet_t *ip = (void*)(_tcp_frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
	if((tcp->data[0]>>4) == 9){ //SUBACK
		//got SUBACK
		printf("SUB: SUBACK\r\n");
		_sub_state++;
		return;
	}
	if((tcp->data[0]>>4) == 3){
		//got PUBLISH
		printf("SUB: Incoming PUBLISH");
		uint8_t *p = tcp->data;
		//uint8_t type = *p >> 4;
		uint16_t remain_len = 0;
		do{
			p++;
			remain_len <<= 7;
			remain_len |= *p&0x7f;
		}while(*p&0x80);
		p += 2;
		uint8_t topic_len = *p; //LSB topic len
		p++; //topic start
		uint8_t topic[20] = {0};
		memcpy(topic, p, topic_len);
		topic[topic_len] = 0; //for safety
		p += topic_len; //data start
		uint8_t data_len = remain_len - (p - (tcp->data+2));
		//uint8_t data[data_len] = {0};
		//memcpy(data, p, data_len);
		uint8_t *data = p;
		data[data_len] = 0; //for safety
		printf(": remain %u,  topic(%u) %s, data(%u) %s\r\n", remain_len, topic_len, topic, data_len, data);
		void (*callb)(mqtt_sub_callback_t*) = NULL;
		for(uint8_t i = 0; i < 5; i++){
			if(!(_sub_subs[i].topic[0]))
				continue;
			char *plus = strchr(_sub_subs[i].topic, '+');
			if(plus){
				//mask parsing
				if(!memcmp(_sub_subs[i].topic, topic, (plus - _sub_subs[i].topic))){
					//gotcha
					callb = _sub_subs[i].callback;
					break;
				}
			}else{
				if(!memcmp(_sub_subs[i].topic, topic, topic_len+1)){
					//gotcha
					callb = _sub_subs[i].callback;
					break;
				}
			}
		}
		if(!callb)
			return;
		mqtt_sub_callback_t callb_data;
		callb_data.topic = (char*)topic;
		callb_data.msg = (char*)data;
		callb(&callb_data);
	}
}

void _sub_reset(){
	_sub_state = 0;
	_sub_time = 0;
}

// MQTT HIGH LEVEL PART ==============================================
uint8_t _all_ready(){
	return _lan_ready() && _tcp_ready() && _auth_ready() && _sub_ready();
}
// MQTT PUBLISH PART
//vars
uint8_t _pub_lb = 0xff;
uint8_t _pub_send_in_progress = 0;
uint32_t _pub_send_time = 0; //msecond timer
uint8_t _pub_packet[55] = {0};
//funcs
void _pub_init(){
	lb_init();
	_pub_lb = lb_alloc(50);
	if(_pub_lb > 0xf0){
		while(1);
	}
	lb_clean(_pub_lb);
	_pub_send_in_progress = 0;
}

void _pub_do(){
	if(_pub_send_in_progress){
		//check timeout
		if(count_1khz() - _pub_send_time > 1000){
			//timeout exceeded - need resend
			printf("PUB: Timeout, resend\r\n");
			_tcp_send(_pub_packet, _pub_packet[49]);
			_pub_send_time = count_1khz();
			_pub_send_in_progress = 1;
		}
	}else{
		if(!lb_is_free(_pub_lb)){
			//got data to send
			if(lb_get(_pub_lb, _pub_packet)){
				printf("PUB: Send\r\n");
				_tcp_send(_pub_packet, _pub_packet[49]);
				_pub_send_time = count_1khz();
				_pub_send_in_progress = 1;
			}
		}
	}
}

void _pub_callback(uint16_t len){
	_pub_send_in_progress = 0;
	if(len){
		//got incoming data
		//not our part
	}else{
		//just ack from any incoming packet
		if(!lb_is_free(_pub_lb)){
			//got data to send
			if(lb_get(_pub_lb, _pub_packet)){
				printf("PUB: ACK Send\r\n");
				_tcp_send(_pub_packet, _pub_packet[49]);
				_pub_send_time = count_1khz();
				_pub_send_in_progress = 1;
			}
		}
	}
}

void _pub_reset(){
	lb_clean(_pub_lb);
	_pub_send_in_progress = 0;
	_pub_send_time = 0;
}

//MQTT PING PART
//vars
uint32_t _ping_time = 0;
uint8_t _ping_success = 0xff;
//funcs
void _ping_init(){
	_ping_time = 0;
	_ping_success = 0xff;
}

void _ping_do(){
	if(_ping_success == 0xff){
		//ping is reset
		if(count_1hz() - _ping_time > 5 || _ping_time == 0){
			//time to send ping
			uint8_t packet[] = {0xc0, 0x00};
			printf("PING: Send\r\n");
			_tcp_send(packet, 2);
			_ping_time = count_1hz();
			_ping_success = 0;
		}
		return;
	}else if(_ping_success == 0){
		//ping sent and waiting answer
		if(count_1hz() - _ping_time > 2){
			//timeout exceeded - need reset
			printf("PING: Timeout\r\n");
			printf("PING: TCP Reset\r\n");
			_tcp_reset();
		}
		return;
	}else{ //on success ping
		_ping_success = 0xff;
		_ping_time = count_1hz();
		//and waiting for next ping time
	}
}

void _ping_callback(){
	ip_packet_t *ip = (void*)(_tcp_frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
	if((tcp->data[0]>>4) == 13){ //PINGRESP
		//got PINGRESP
		printf("PING: PINGRESP\r\n");
		_ping_success = 1;
	}
}

void _ping_reset(){
	_ping_time = 0;
	_ping_success = 0xff;
}

// LAN CALLBACKS ======================================================
uint8_t tcp_listen(uint8_t id, eth_frame_t *frame){
	return 0;
}

void tcp_read(uint8_t id, eth_frame_t *frame, uint8_t re){
//	ip_packet_t *ip = (void*)(frame->data);
//	tcp_packet_t *tcp = (void*)(ip->data);
	_tcp_recv_time = count_1hz();
	if(id != _tcp_socket)
		return;
	//NOTE: no need to give eth frame to callbacks,
	//it is accessed through *orig_frame
	if(_all_ready()){
		_pub_callback(0);
	}
	led_blink(LED1);
}

void tcp_write(uint8_t id, eth_frame_t *frame, uint16_t len){
	ip_packet_t *ip = (void*)(frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
	_tcp_recv_time = count_1hz();
	if(id != _tcp_socket)
		return;
	//printf("Incoming packet type: %u\r\n", tcp->data[0]>>4);
	//NOTE: no need to give eth frame to callbacks,
	//it is accessed through *orig_frame
	if(!_tcp_ready()){
		//early packet, TCP is just establishing
		//nothing to do yet
	}else if(!_auth_ready()){
		//some auth answers
		_auth_callback();
	}else if(!_sub_ready()){
		//some subscription answers
		_sub_callback();
	}else{
		_sub_callback();
		_ping_callback();
		_pub_callback(len);
	}
}

void tcp_closed(uint8_t id, uint8_t hard){
	if(id == _tcp_socket)
		_tcp_socket = 0xff;
}

// PUBLIC FUNCTIONS =====================================================

void mw_init(){
	printf("LAN MQTT Wrapper Init\r\n");
	_lan_init();
	_tcp_init();
	_auth_init();
	_sub_init();
	_pub_init();
	_ping_init();
}

void mw_status(){
	printf("LAN: %s TCP: %s AUTH: %s SUB: %s -- %s\r\n",
			(_lan_ready()?"OK":"Fail"),
			(_tcp_ready()?"OK":"Fail"),
			(_auth_ready()?"OK":"Fail"),
			(_sub_ready()?"OK":"Fail"),
			(_all_ready()?"OK":"Fail"));
}

void mw_poll(){
	static uint32_t led_update = 0;
	lan_poll();
	if(!_lan_ready()){
		_tcp_reset();
		return;
	}else if(!_tcp_ready()){
		_tcp_connect();
		_auth_reset();
	}else if(!_auth_ready()){
		_auth_do();
		_sub_reset();
	}else if(!_sub_ready()){
		_sub_do();
		_ping_reset();
	}else{
		_pub_do();
		_ping_do();
	}
	if(led_update == 0 || count_1khz() - led_update > 1000){
		if(_all_ready())
			led_on(LED1);
		else
			led_off(LED1);
		led_update = count_1khz();
	}
}

void mw_publish(char *topic, char *msg){
	if(lb_is_full(_pub_lb))
		return;
	uint8_t packet[55];
	uint8_t packet_len = mqtt_publish(packet, topic, msg);
	packet[49] = packet_len;
	if(!lb_put(_pub_lb, packet))
		return;
}

void mw_subscribe(char *topic, void (*callb)(mqtt_sub_callback_t*)){
	if(_sub_subs_num == 5)
		return;
	mqtt_sub_t *sub = &(_sub_subs[_sub_subs_num]);
	strcpy(sub->topic, topic);
	sub->callback = callb;
	_sub_subs_num++;
}
