/*
 *  libmqtt.h
 *  libmqtt
 *
 *  Created by Filipe Varela on 09/10/16.
 *  Copyright 2009 Caixa Mágica Software. All rights reserved.
 *
 */

#ifndef _LIBMQTT_H_
#define _LIBMQTT_H_

#include <stdint.h>

#define KEEPALIVE 15000
#define MQTTCONNECT 1<<4
#define MQTTPUBLISH 3<<4
#define MQTTSUBSCRIBE 8<<4
#define MQTTPING 12<<4
#define MQTTPINGRESP 13<<4

uint8_t mqtt_connect(uint8_t *packet, const char *clientid);
uint8_t mqtt_publish(uint8_t *packet, const char *topic, char *msg);
uint8_t mqtt_subscribe(uint8_t *packet, const char *topic);
//uint8_t mqtt_ping(mqtt_broker_handle_t *broker);
//void mqtt_callback(mqtt_broker_handle_t *broker, uint8_t *buffer, uint8_t len);

#endif /* _LIBMQTT_H_ */
