/*
 * lan_utils.h
 *
 *  Created on: 13.06.2014
 *      Author: Admin
 */

#ifndef LAN_UTILS_H_
#define LAN_UTILS_H_
#include <stdint.h>

uint8_t lu_ping(uint32_t addr);
void lu_dhcp_reset(void);


#endif /* LAN_UTILS_H_ */
