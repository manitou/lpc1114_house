################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/lan_utils.c \
../src/libmqtt.c \
../src/mqtt_wrapper.c 

OBJS += \
./src/lan_utils.o \
./src/libmqtt.o \
./src/mqtt_wrapper.o 

C_DEPS += \
./src/lan_utils.d \
./src/libmqtt.d \
./src/mqtt_wrapper.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M0 -D__LPC11XX__ -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx" -I"C:\LPC1114_GIT\core" -I"C:\LPC1114_GIT\enc28j60" -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx\inc" -I"C:\LPC1114_GIT\mqtt" -I"C:\LPC1114_GIT\mqtt\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -std=c99 -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


