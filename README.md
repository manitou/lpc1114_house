# README #

This is software kit for building your own smart home system based on LPC1114 ARM Cortex M0 controller. 
Here we have basic peripheral library, some libraries for communication interfaces and libraries producing interconnection layers.

### Purpose ###

* Smart wireless house monitoring and control
* Remote control
* Tiny and cheap IP devices

### Quick setup ###

* Take LPCXpresso IDE from official site
* Clone repository to some folder
* Import projects into workspace
* Build

More information available at [dev.plus.zt.ua](http://dev.plus.zt.ua/wiki/doku.php?id=lpc1114)