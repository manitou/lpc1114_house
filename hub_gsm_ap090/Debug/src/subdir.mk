################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/cr_startup_lpc11xx.c \
../src/crp.c \
../src/irq_handlers.c \
../src/main.c 

OBJS += \
./src/cr_startup_lpc11xx.o \
./src/crp.o \
./src/irq_handlers.o \
./src/main.o 

C_DEPS += \
./src/cr_startup_lpc11xx.d \
./src/crp.d \
./src/irq_handlers.d \
./src/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -DCORE_M0 -D__USE_CMSIS=CMSIS_CORE_LPC11xx -D__LPC11XX__ -I"C:\LPC1114_GIT\mqtt_gsm" -I"C:\LPC1114_GIT\rfm73_server" -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx" -I"C:\LPC1114_GIT\core" -I"C:\LPC1114_GIT\ap090" -I"C:\LPC1114_GIT\CMSIS_CORE_LPC11xx\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -std=c99 -mcpu=cortex-m0 -mthumb -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


